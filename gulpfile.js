const gulp = require('gulp');
const del = require('del');
const runSequence = require('run-sequence');
const eslint = require('gulp-eslint');
const sass = require('gulp-sass');
const autoPrefixer = require('gulp-autoprefixer');
const notify = require('gulp-notify');
const browserSync = require('browser-sync');
const gulpIf = require('gulp-if');
const gutil = require('gulp-util');
const source = require('vinyl-source-stream');
const streamify = require('gulp-streamify');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const watchify = require('watchify');
const browserify = require('browserify');
const babelify = require('babelify');
const uglify = require('gulp-uglify');
const debowerify = require('debowerify');
const prettier = require('gulp-prettier');

const config = {
    developmentPort: 3000,
    browserSyncPort: 3001,
    sourceDirectory: './app/',
    buildDirectory: './build/',
    scripts: {
        src: './app/**/*.jsx',
        dest: './build/assets/js/'
    },
    js: {
        src: './app/assets/js/**/*.js',
        dest: './build/assets/js/'
    },

    images: {
        src: './app/assets/img/**/*.{jpeg,jpg,png,gif}',
        dest: './build/assets/img/'
    },

    fonts: {
        src: './app/assets/fonts/**/*.{eot,otf,ttc,ttf,woff2}',
        dest: './build/assets/fonts/'
    },

    styles: {
        src: './app/assets/sass/',
        watch: './app/assets/sass/*.scss',
        dest: './build/assets/css/',
        fileName: 'styles.css'
    },

    prettier: {
        singleQuote: true,
        tabWidth: 4,
        arrowParens: 'always'
    }
};

/* Development tasks */
gulp.task('development', ['reset-build-directory'], (callback) => {
    global.isProduction = false;
    return runSequence(
        [
            'sass',
            'browserify',
            'copy-fonts',
            'copy-html',
            'copy-images',
            'copy-js',
            'copy-3d',
            'eslint'
        ],
        'watch',
        callback
    );
});

/* Format tasks */
gulp.task('format', ['reset-build-directory'], (callback) => {
    global.isProduction = false;
    return runSequence(['prettier', 'eslint'], callback);
});

/* Productions tasks */
gulp.task('production', ['reset-build-directory'], (callback) => {
    global.isProduction = true;

    runSequence(
        [
            'sass',
            'browserify',
            'copy-fonts',
            'copy-html',
            'copy-images',
            'copy-js',
            'copy-3d',
            'eslint'
        ],
        'postbuild',
        callback
    );
});

/* Es lint */
gulp.task('eslint', () => {
    return gulp
        .src([config.scripts.src, './*.js'])
        .pipe(eslint({ fix: true }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

/* prettier */
gulp.task('prettier', () => {
    return gulp
        .src([config.scripts.src, './*.js'])
        .pipe(prettier(config.prettier))
        .pipe(gulp.dest((file) => file.base));
});

/* Copy stuff to the build folder */

gulp.task('copy-fonts', () => {
    return gulp
        .src([config.sourceDirectory + 'assets/fonts/**/*'])
        .pipe(gulp.dest(config.buildDirectory + 'assets/fonts/'));
});

gulp.task('copy-images', () => {
    return gulp
        .src([config.sourceDirectory + 'assets/img/**/*'])
        .pipe(gulp.dest(config.buildDirectory + 'assets/img/'));
});

gulp.task('copy-html', () => {
    gulp.src(config.sourceDirectory + '*.html').pipe(
        gulp.dest(config.buildDirectory)
    );
    gulp.src(config.sourceDirectory + 'manifest.json').pipe(
        gulp.dest(config.buildDirectory)
    );
    gulp.src(config.sourceDirectory + 'favicon.ico').pipe(
        gulp.dest(config.buildDirectory)
    );
});

gulp.task('copy-js', () => {
    return gulp
        .src([config.sourceDirectory + 'assets/js/**/*'])
        .pipe(gulp.dest(config.buildDirectory + 'assets/js/'));
});

//This doesn't get watched becaude 3D Elements won't change much
gulp.task('copy-3d', () => {
    return gulp
        .src([config.sourceDirectory + 'assets/3d/**/*'])
        .pipe(gulp.dest(config.buildDirectory + 'assets/3d/'));
});

/* Sass transpiling  */
/* Not using ruby-sass might cause problems with bootstrap */
gulp.task('sass', () => {
    return gulp
        .src(config.styles.src + '/**/*.scss')
        .pipe(
            sass({
                sourceComments: !global.isProduction,
                outputStyle: global.isProduction ? 'compressed' : 'nested',
                includePaths: [
                    './node_modules/bootstrap-sass/assets/stylesheets/'
                ]
            })
        )
        .on('error', handleErrors)
        .pipe(autoPrefixer('last 2 versions', '> 1%', 'ie 8'))
        .pipe(rename(config.styles.fileName))
        .pipe(gulp.dest(config.styles.dest))
        .pipe(gulpIf(browserSync.active, browserSync.reload({ stream: true })));
});

/* Watch for changes and trigger tasks */
gulp.task('watch', ['browser-sync'], () => {
    gulp.watch(config.scripts.src, ['eslint']);
    gulp.watch(config.styles.watch, ['sass']);
    gulp.watch(config.images.src, ['copy-images']);
    gulp.watch(config.sourceDirectory + '*.html', ['copy-html']);
    gulp.watch(config.fonts.src, ['copy-fonts']);
    gulp.watch(config.js.src, ['copy-js']);
});

/* Used for live change previews */
gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: config.buildDirectory
        },
        port: config.developmentPort,
        ui: {
            port: config.browserSyncPort
        },
        ghostMode: {
            links: false
        }
    });
});

/* Reset build directory */
gulp.task('reset-build-directory', () => {
    return del([config.buildDirectory]);
});

/* Remove this eventually, this is 100x more complicated than it needs to be */
function handleErrors() {
    if (!global.isProduction) {
        let args = Array.prototype.slice.call(arguments);

        notify
            .onError({
                title: 'Gulp Compile Error',
                message: '<%= error.message %>'
            })
            .apply(this, args);

        this.emit('end');
    } else {
        console.log(error);
        process.exit(1);
    }
}

/* Babel + Browserify + Uglify */
function buildScript(file, watch) {
    let bundler = browserify({
        extensions: ['.js', '.jsx'],
        entries: [config.sourceDirectory + '' + file],
        debug: !global.isProduction,
        cache: {},
        packageCache: {},
        fullPaths: true
    });

    const rebundle = () => {
        let stream = bundler.bundle();

        gutil.log('Rebuilding JavaScript Bundle');

        return stream
            .on('error', handleErrors)
            .pipe(source(file))
            .pipe(gulpIf(global.isProduction, streamify(uglify())))
            .pipe(streamify(rename('main.js')))
            .pipe(gulpIf(!global.isProduction, sourcemaps.write('./')))
            .pipe(gulp.dest(config.scripts.dest))
            .pipe(
                gulpIf(
                    browserSync.active,
                    browserSync.reload({ stream: true, once: true })
                )
            );
    };

    if (watch) {
        bundler = watchify(bundler);
        bundler.on('update', rebundle);
    }

    bundler.transform(babelify, { presets: ['es2015', 'react', 'stage-0'] });
    bundler.transform(debowerify);

    return rebundle();
}

gulp.task('browserify', () => {
    return buildScript('index.jsx', !global.isProduction);
});

gulp.task('postbuild', () => {
    console.log('No postbuild tasks to perform.');
});
