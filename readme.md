Brainstorm is an interactive web application that can visualize EEG data in real-time.


## Install Guide

### Install Reqs
[git](https://git-scm.com), [nodejs 6+](https://nodejs.org/en/)

### Clone Repo
```sh
git clone https://gitlab.com/MisterEz/brain-app.git
```

### Install
```sh
npm install
```

### Development Build
```sh
npm start
```

### Production Build
```sh
npm run build
```
