import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClose = (e) => {
        e.preventDefault();

        let firstClass = e.target.className.split(' ')[0];
        if (firstClass === 'modal') {
            this.props.onClose(e);
        }
    };

    render() {
        let closeBtn = null;
        if (this.props.closeable) {
            closeBtn = (
                <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.props.onClose}
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            );
        }

        return (
            <div>
                <div
                    className={[
                        'modal',
                        'fade',
                        'in',
                        this.props.className
                    ].join(' ')}
                    tabIndex="-1"
                    role="dialog"
                    style={{ display: 'block' }}
                    onClick={this.props.closeable ? this.handleClose : null}
                >
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                {closeBtn}
                                {this.props.header}
                            </div>
                            <div className="modal-body">{this.props.body}</div>
                            <div className="modal-footer">
                                {this.props.footer}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade in" />
            </div>
        );
    }
}

Modal.propTypes = {
    className: PropTypes.string,
    header: PropTypes.node,
    body: PropTypes.node,
    footer: PropTypes.node,
    onClose: PropTypes.func,
    closeable: PropTypes.bool
};

Modal.defaultProps = {
    className: '',
    body: <h2>No Header</h2>,
    body: <div>No Body</div>,
    footer: (
        <button type="button" className="btn btn-primary">
            Ok
        </button>
    ),
    onClose: () => {},
    closeable: true
};

export default Modal;
