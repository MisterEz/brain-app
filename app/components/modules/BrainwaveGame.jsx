import React from 'react';
import PropTypes from 'prop-types';
import ModuleOptions from '../ModuleOptions';
import { resizeComponents } from '../../Helpers.jsx';

class BrainwaveGame extends React.Component {
    constructor(props) {
        super(props);
        this.containerRef = React.createRef();
        this.canvasBack = React.createRef();
        this.canvasMiddle = React.createRef();
        this.canvasFront = React.createRef();
        this.bannerRef = React.createRef();
        this.arrowRef = React.createRef();

        let width = 925;
        let height = 525;
        this.rightOffset = 110;
        let bannerOffset = 1050;

        this.nextNode = 1;
        this.pastNode = 0;
        this.axis = 'x';
        this.polarity = '+';
        this.turnDirection = 'l';
        this.position = { x: 44, y: 44 };
        this.gameRunning = false;
        this.xOffset = (0.01 * width) / 2;
        this.yOffset = -(0.05 * height) / 2;
        this.angle = 90;
        this.collectableCount = 5;
        this.isRedrawn = true;

        this.mainTotal = 0;
        this.alphaTotal = 0;
        this.calibrationCount = 0;
        this.maxCallibrationCount = 0;
        this.beginCalibration = false;
        this.aboveThreshold;
        this.eyesClosed = false;
        this.lastMode = 'faceMode';

        this.background = new Image();
        this.background.src = '../../assets/img/Brainwave_Game/background.png';

        this.ship = new Image();
        this.ship.src = '../../assets/img/Brainwave_Game/ship.png';

        this.directionArrow = new Image();
        this.directionArrow.src =
            '../../assets/img/Brainwave_Game/directionArrow.png';

        this.collectable = new Image();
        this.collectable.src =
            '../../assets/img/Brainwave_Game/collectable.png';

        this.mapNodes = [
            { x: 33, y: 44, connected: [1, 5] },
            { x: 352, y: 44, connected: [0, 6, 2] },
            { x: 971, y: 44, connected: [1, 4] },
            { x: 702, y: 217, connected: [4, 7] },
            { x: 971, y: 217, connected: [2, 3, 11] },
            { x: 33, y: 386, connected: [0, 6, 8] },
            { x: 352, y: 386, connected: [5, 1, 7] },
            { x: 702, y: 386, connected: [6, 3, 10] },
            { x: 33, y: 657, connected: [5, 9, 12] },
            { x: 352, y: 657, connected: [8, 10, 13] },
            { x: 702, y: 657, connected: [9, 11, 7] },
            { x: 971, y: 657, connected: [10, 4, 14] },
            { x: 33, y: 950, connected: [8, 13] },
            { x: 352, y: 950, connected: [12, 9, 14] },
            { x: 971, y: 950, connected: [13, 11] }
        ];

        this.collectables = [
            { x: 480, y: 386 },
            { x: 800, y: 217 },
            { x: 871, y: 657 },
            { x: 352, y: 217 },
            { x: 352, y: 830 }
        ];

        this.remainingCollectables = this.collectables;

        if (typeof window !== 'undefined') {
            window.addEventListener('keypress', this.handleKeypress);
            window.addEventListener('bciUpdate', this.bciUpdate);
        }

        if (this.props.setOptionsCallback) {
            this.props.setOptionsCallback(
                function() {
                    this.setState((prevState) => {
                        return {
                            optionsOpen: !prevState.optionsOpen
                        };
                    });
                }.bind(this)
            );
        }

        this.state = {
            time: 0,
            width: width,
            height: height,
            gameOffset: this.rightOffset,
            bannerOffset: bannerOffset,
            isResizing: false,
            nextDirection: 'Up',
            mode: 'faceMode',
            ranger: {},
            threshold: 2,
            ignoreThreshold: 2,
            text: 'Calibration required',
            gameState: 'Start Game',
            calibrateText: 'Calibrate',
            bestTimes: [
                {
                    type: 'text',
                    text: '259.66'
                },
                {
                    type: 'hr'
                }
            ],
            optionsOpen: false
        };

        this.gameStyle = 'centre';
        this.bannerStyle = 'gameBanner';
        this.timeStyle = 'time';
        this.directionStyle = 'directionText';
        this.startStyle = 'startButton';
        this.resetStyle = 'resetButton';
        this.descriptionText = (
            <label className="description">
                Navigate the asteroid field and collect the gems as quickly as
                you can!!!
            </label>
        );
        this.arrow = (
            <canvas
                className="arrow"
                width={100}
                height={100}
                ref={this.arrowRef}
            />
        );

        if (window.Compatibility.isMobile) {
            this.gameStyle = 'mobileGame';
            this.bannerStyle = 'mobileBanner';
            this.timeStyle = 'mobileTime';
            this.directionStyle = 'mobileDirectionText';
            this.startStyle = 'mobileStartButton';
            this.resetStyle = 'mobileResetButton';
            this.descriptionText = '';
            this.arrow = '';
            this.rightOffset = 0;
            bannerOffset = 0;
        }
    }
    //don't forget to put 'e' back in once this starts to be implemented.
    bciUpdate = (e) => {
        let mainAverage = 0;
        let alphaAverage = 0;
        let sensorLength = 0;
        let frequencyLength = 0;

        for (let sensor in e.detail.channels) {
            if (e.detail.channels.hasOwnProperty(sensor)) {
                frequencyLength = 0;
                for (let frequency in e.detail.channels[sensor]) {
                    if (frequency === 'alpha') {
                        alphaAverage += e.detail.channels[sensor][frequency];
                    } else {
                        mainAverage += e.detail.channels[sensor][frequency];
                        frequencyLength++;
                    }
                }
                sensorLength++;
            }
        }

        mainAverage /= sensorLength * frequencyLength;
        alphaAverage /= sensorLength;

        if (
            this.beginCalibration &&
            this.calibrationCount < this.maxCallibrationCount
        ) {
            this.calibrationCount++;
            this.mainTotal += mainAverage;
            this.alphaTotal += alphaAverage;
            this.setState({
                text:
                    'Calibration in progress: ' +
                    (this.maxCallibrationCount - this.calibrationCount) / 10
            });
        }

        if (
            this.beginCalibration &&
            this.calibrationCount === this.maxCallibrationCount &&
            this.maxCallibrationCount > 0
        ) {
            this.beginCalibration = false;
            this.setState({ text: 'Calibration complete' });
        }

        switch (this.state.mode) {
            case 'eyesMode':
                if (
                    alphaAverage >
                        (this.alphaTotal / this.maxCallibrationCount) *
                            (this.state.threshold / 2) &&
                    mainAverage <
                        (this.mainTotal / this.maxCallibrationCount) *
                            (this.state.ignoreThreshold / 2)
                ) {
                    this.eyesClosed = true;
                } else {
                    if (this.eyesClosed) {
                        this.switchDirection();
                    }
                    this.eyesClosed = false;
                }
                break;
            case 'faceMode':
                if (
                    mainAverage >
                    (this.mainTotal / this.maxCallibrationCount) *
                        (this.state.threshold / 2)
                ) {
                    this.eyesClosed = true;
                } else {
                    if (this.eyesClosed) {
                        this.switchDirection();
                    }
                    this.eyesClosed = false;
                }
                break;
        }
    };
    switchDirection = () => {
        switch (this.turnDirection) {
            case 'l':
                this.turnDirection = 'r';
                break;
            case 'r':
                this.turnDirection = 'l';
                break;
        }
        this.setDirectionText();
    };
    calibrate = () => {
        if (this.state.calibrateText !== 'Stop Calibration') {
            this.beginCalibration = true;
            this.maxCallibrationCount += 50;
            this.setState({ calibrateText: 'Stop Calibration' });
        } else {
            this.maxCallibrationCount = this.calibrationCount;
            this.beginCalibration = false;
            this.setState({
                calibrateText: 'Calibrate',
                text: 'Calibration complete'
            });
        }
    };
    reset = () => {
        this.beginCalibration = false;
        this.maxCallibrationCount = 0;
        this.calibrationCount = 0;
        this.mainTotal = 0;
        this.alphaTotal = 0;
        this.setState({ text: 'Calibration required' });
    };
    drawCollectables = () => {
        let context = this.canvasMiddle.current.getContext('2d');

        for (let i = 0; i < this.remainingCollectables.length; i++) {
            context.drawImage(
                this.collectable,
                (this.remainingCollectables[i].x / 1000) * this.state.width -
                    (0.015 * this.state.width) / 2,
                (this.remainingCollectables[i].y / 1000) * this.state.height -
                    (0.02 * 925) / 2,
                0.015 * this.state.width,
                0.02 * 925
            );
        }
    };
    clearCollectable = (i) => {
        let context = this.canvasMiddle.current.getContext('2d');
        let width = this.state.width;
        let height = this.state.height;
        context.clearRect(
            (this.remainingCollectables[i].x / 1000) * width - 10,
            (this.remainingCollectables[i].y / 1000) * height - 10,
            (this.remainingCollectables[i].x / 1000) * width + 10,
            (this.remainingCollectables[i].y / 1000) * height + 10
        );
    };
    drawPlayer = () => {
        let context = this.canvasFront.current.getContext('2d');
        let width = this.state.width;
        let height = this.state.height;

        context.clearRect(0, 0, width, height);

        context.save();

        context.translate(
            (this.position.x / 1000) * this.state.width -
                (0.02 * this.state.width) / 2,
            (this.position.y / 1000) * this.state.height -
                (0.05 * this.state.height) / 2
        );

        context.rotate((this.angle * Math.PI) / 180);

        if (this.axis === 'x' && this.state.width < 925) {
            width = 925;
        }

        context.drawImage(
            this.ship,
            this.xOffset,
            this.yOffset,
            0.02 * width,
            0.05 * this.state.height
        );

        context.restore();
    };
    handleKeypress = (e) => {
        var key;

        if (window.event) {
            // IE
            key = e.keyCode;
        } else if (e.which) {
            // Netscape/Firefox/Opera
            key = e.which;
        }

        if (String.fromCharCode(key) === 's') {
            this.switchDirection();
        }

        //this.setDirectionText();
    };
    setDirectionText() {
        let context = this.arrowRef.current.getContext('2d');
        let xOffset, yOffset;

        context.clearRect(0, 0, 100, 100);
        context.save();

        switch (this.turnDirection + this.axis + this.polarity) {
            case 'lx+':
                xOffset = 0;
                yOffset = 0;
                context.rotate((0 * Math.PI) / 180);
                break;
            case 'lx-':
                xOffset = -100;
                yOffset = -100;
                context.rotate((180 * Math.PI) / 180);
                break;
            case 'ly+':
                xOffset = 0;
                yOffset = -100;
                context.rotate((90 * Math.PI) / 180);
                break;
            case 'ly-':
                xOffset = -100;
                yOffset = 0;
                context.rotate((270 * Math.PI) / 180);
                break;
            case 'rx+':
                xOffset = -100;
                yOffset = -100;
                context.rotate((180 * Math.PI) / 180);
                break;
            case 'rx-':
                xOffset = 0;
                yOffset = 0;
                context.rotate((0 * Math.PI) / 180);
                break;
            case 'ry+':
                xOffset = -100;
                yOffset = 0;
                context.rotate((270 * Math.PI) / 180);
                break;
            case 'ry-':
                xOffset = 0;
                yOffset = -100;
                context.rotate((90 * Math.PI) / 180);
                break;
        }
        context.drawImage(this.directionArrow, xOffset, yOffset, 100, 100);
        context.restore();
    }
    movePlayer = () => {
        if (
            this.position.x !== this.mapNodes[this.nextNode].x ||
            this.position.y !== this.mapNodes[this.nextNode].y
        ) {
            if (this.axis === 'x') {
                if (this.polarity === '+') {
                    this.position.x += 1;
                } else {
                    this.position.x -= 1;
                }
            } else {
                if (this.polarity === '+') {
                    this.position.y += 1;
                } else {
                    this.position.y -= 1;
                }
            }

            let newRemaining = [];
            for (let i = 0; i < this.remainingCollectables.length; i++) {
                if (
                    this.position.y === this.remainingCollectables[i].y &&
                    this.position.x === this.remainingCollectables[i].x
                ) {
                    this.clearCollectable(i);
                } else {
                    newRemaining.push(this.remainingCollectables[i]);
                }
            }

            if (newRemaining.length === 0) {
                this.endGame();
            } else {
                this.remainingCollectables = newRemaining;
                this.drawCollectables();
            }

            this.drawPlayer();
        } else {
            let connected = this.mapNodes[this.nextNode].connected;
            let y = this.mapNodes[this.nextNode].y;
            let x = this.mapNodes[this.nextNode].x;

            if (connected.length > 2) {
                let lowerX,
                    higherX,
                    lowerY,
                    higherY,
                    newNode,
                    newAxis,
                    newPolarity,
                    newAngle,
                    xOffset,
                    yOffset;

                for (let i = 0; i < connected.length; i++) {
                    if (this.mapNodes[connected[i]].x < x) {
                        lowerX = connected[i];
                    } else if (this.mapNodes[connected[i]].x > x) {
                        higherX = connected[i];
                    } else if (this.mapNodes[connected[i]].y < y) {
                        lowerY = connected[i];
                    } else if (this.mapNodes[connected[i]].y > y) {
                        higherY = connected[i];
                    }
                }

                //Change variables when can turn in the desired direction
                switch (this.turnDirection + this.axis + this.polarity) {
                    case 'lx+':
                        newNode = lowerY;
                        newAngle = 0;
                        newAxis = 'y';
                        newPolarity = '-';
                        xOffset = 0;
                        yOffset = 0;
                        break;
                    case 'lx-':
                        newNode = higherY;
                        newAngle = 180;
                        newAxis = 'y';
                        newPolarity = '+';
                        xOffset = -((0.04 * this.state.width) / 2);
                        yOffset = -(0.1 * this.state.height) / 2;
                        break;
                    case 'ly+':
                        newNode = higherX;
                        newAngle = 90;
                        newAxis = 'x';
                        newPolarity = '+';
                        xOffset = (0.01 * 925) / 2;
                        yOffset = -(0.05 * this.state.height) / 2;
                        break;
                    case 'ly-':
                        newNode = lowerX;
                        newAngle = 270;
                        newAxis = 'x';
                        newPolarity = '-';
                        xOffset = -(0.05 * 925) / 2;
                        yOffset = -(0.02 * this.state.height) / 2;
                        break;
                    case 'rx+':
                        newNode = higherY;
                        newAngle = 180;
                        newAxis = 'y';
                        newPolarity = '+';
                        xOffset = -((0.04 * this.state.width) / 2);
                        yOffset = -(0.1 * this.state.height) / 2;
                        break;
                    case 'rx-':
                        newNode = lowerY;
                        newAngle = 0;
                        newAxis = 'y';
                        newPolarity = '-';
                        xOffset = 0;
                        yOffset = 0;
                        break;
                    case 'ry+':
                        newNode = lowerX;
                        newAngle = 270;
                        newAxis = 'x';
                        newPolarity = '-';
                        xOffset = -(0.05 * 925) / 2;
                        yOffset = -(0.02 * this.state.height) / 2;
                        break;
                    case 'ry-':
                        newNode = higherX;
                        newAngle = 90;
                        newAxis = 'x';
                        newPolarity = '+';
                        xOffset = (0.01 * 925) / 2;
                        yOffset = -(0.05 * this.state.height) / 2;
                        break;
                }

                //Keep going straight if the desired direction is not available yet
                if (newNode === undefined) {
                    switch (this.axis + this.polarity) {
                        case 'x+':
                            newNode = higherX;
                            break;
                        case 'x-':
                            newNode = lowerX;
                            break;
                        case 'y+':
                            newNode = higherY;
                            break;
                        case 'y-':
                            newNode = lowerY;
                            break;
                    }

                    this.pastNode = this.nextNode;
                    this.nextNode = newNode;
                } else {
                    this.axis = newAxis;
                    this.polarity = newPolarity;
                    this.pastNode = this.nextNode;
                    this.nextNode = newNode;
                    this.xOffset = xOffset;
                    this.yOffset = yOffset;
                    this.angle = newAngle;
                }

                //perform turn when there is only one option.
            } else {
                let newNode = connected[0];

                if (newNode === this.pastNode) {
                    newNode = connected[1];
                }

                if (this.mapNodes[newNode].y < y) {
                    this.angle = 0;
                    this.axis = 'y';
                    this.polarity = '-';
                    this.xOffset = 0;
                    this.yOffset = 0;
                } else if (this.mapNodes[newNode].y > y) {
                    this.angle = 180;
                    this.axis = 'y';
                    this.polarity = '+';
                    this.xOffset = -((0.04 * this.state.width) / 2);
                    this.yOffset = -(0.1 * this.state.height) / 2;
                } else if (this.mapNodes[newNode].x < x) {
                    this.angle = 270;
                    this.axis = 'x';
                    this.polarity = '-';
                    this.xOffset = -(0.05 * 925) / 2;
                    this.yOffset = -(0.02 * this.state.height) / 2;
                } else {
                    this.angle = 90;
                    this.axis = 'x';
                    this.polarity = '+';
                    this.xOffset = (0.01 * 925) / 2;
                    this.yOffset = -(0.05 * this.state.height) / 2;
                }

                this.pastNode = this.nextNode;
                this.nextNode = newNode;
            }

            this.setDirectionText();
        }
    };
    runUpdate = () => {
        let time = this.state.time;
        time += 0.02;
        this.setState({ time: Number(time.toFixed(2)) });
        this.movePlayer();
    };
    startGame = () => {
        if (this.gameRunInterval === undefined) {
            this.gameRunInterval = setInterval(this.runUpdate, 20);
            this.setState({ gameState: 'Pause Game' });
        } else {
            clearInterval(this.gameRunInterval);
            this.gameRunInterval = undefined;
            this.setState({ gameState: 'Resume Game' });
        }
    };
    resetGame = () => {
        clearInterval(this.gameRunInterval);

        this.gameRunInterval = undefined;
        this.nextNode = 1;
        this.pastNode = 0;
        this.axis = 'x';
        this.polarity = '+';
        this.turnDirection = 'l';
        this.position = { x: 44, y: 44 };
        this.collectableCount = 5;
        this.angle = 90;
        this.xOffset = (0.01 * this.state.width) / 2;
        this.yOffset = -(0.05 * this.state.height) / 2;
        this.remainingCollectables = this.collectables;

        this.setDirectionText();
        this.drawCollectables();
        this.drawPlayer();
        this.setDirectionText();
        this.setState({
            time: 0,
            gameState: 'Start Game'
        });
    };
    endGame = () => {
        clearInterval(this.gameRunInterval);
        let bestTimes = this.state.bestTimes;

        bestTimes.push({
            type: 'text',
            text: this.state.time
        });
        bestTimes.push({
            type: 'hr'
        });

        this.setState({ bestTimes: bestTimes });
    };
    updateThreshold = (e) => {
        this.setState({ threshold: e.target.value });
    };
    updateIgnoreThreshold = (e) => {
        this.setState({ ignoreThreshold: e.target.value });
    };
    getOptions = () => {
        return [
            {
                type: 'submenu',
                text: 'Top Times',
                desc: 'Temporary list of top times',
                options: [
                    {
                        type: 'list',
                        list: this.state.bestTimes
                    }
                ]
            },
            {
                type: 'text',
                text: this.state.text
            },
            {
                type: 'button',
                name: 'calibrateButton',
                text: this.state.calibrateText,
                onClick: this.calibrate
            },
            {
                type: 'button',
                name: 'reset',
                text: 'Reset',
                onClick: this.reset
            },
            {
                type: 'buttonGroup',
                text: 'BCI Mode',
                desc: 'Choose action for BCI',
                name: 'mode',
                value: this.state.mode,
                btns: [
                    {
                        type: 'button',
                        name: 'eyesMode',
                        text: 'Eyes Closed'
                    },
                    {
                        type: 'button',
                        name: 'faceMode',
                        text: 'Face Muscles'
                    }
                ]
            },
            {
                type: 'ranger',
                text: 'Threshold',
                desc: 'Threshold: ' + this.state.threshold / 2,
                name: 'width',
                min: 1,
                max: 80,
                value: this.state.threshold,
                onChange: this.updateThreshold
            },
            this.state.ranger
        ];
    };
    optionsCallback = (newState) => {
        this.setState(newState);
    };
    render() {
        return (
            <div ref={this.containerRef}>
                <div id="BRAINWAVE_GAME" className="moduleHeightStretch">
                    <div
                        ref={this.bannerRef}
                        className={this.bannerStyle}
                        style={{ left: this.state.bannerOffset }}
                    >
                        <button
                            className={this.startStyle + ' btn btn-primary'}
                            onClick={this.startGame}
                        >
                            {this.state.gameState}
                        </button>
                        <button
                            className={this.resetStyle + ' btn btn-primary'}
                            onClick={this.resetGame}
                        >
                            Reset
                        </button>
                        <label className={this.timeStyle}>
                            {'Time: ' + this.state.time}
                        </label>
                        <label className={this.directionStyle}>
                            {'Next Direction: '}
                        </label>
                        {this.descriptionText}
                        {this.arrow}
                    </div>
                    <canvas
                        className={'layer1 ' + this.gameStyle}
                        width={this.state.width}
                        height={this.state.height}
                        ref={this.canvasBack}
                        style={{ left: this.state.gameOffset }}
                    />
                    <canvas
                        className={'layer2 ' + this.gameStyle}
                        width={this.state.width}
                        height={this.state.height}
                        ref={this.canvasMiddle}
                        style={{ left: this.state.gameOffset }}
                    />
                    <canvas
                        className={'layer3 ' + this.gameStyle}
                        width={this.state.width}
                        height={this.state.height}
                        ref={this.canvasFront}
                        style={{ left: this.state.gameOffset }}
                    />
                </div>
                <ModuleOptions
                    open={this.state.optionsOpen}
                    options={this.getOptions()}
                    optionsCallback={this.optionsCallback}
                />
            </div>
        );
    }
    componentDidUpdate() {
        if (
            this.state.mode !== this.lastMode &&
            this.state.mode === 'eyesMode'
        ) {
            let ranger = {
                type: 'ranger',
                text: 'Ignore Threshold',
                desc:
                    'Threshold for when to ignore alpha value. \nThreshold: ' +
                    this.state.ignoreThreshold / 2,
                name: 'height',
                min: 1,
                max: 80,
                value: this.state.ignoreThreshold,
                onChange: this.updateIgnoreThreshold
            };
            this.lastMode = this.state.mode;
            this.setState({ ranger: ranger });
        } else if (
            this.state.mode !== this.lastMode &&
            this.state.mode === 'faceMode'
        ) {
            this.lastMode = this.state.mode;
            this.setState({ ranger: {} });
        }

        //Start resizing
        if (this.props.isResizing && !this.state.isResizing) {
            this.setState({
                isResizing: true
            });
        }
        //End resizing
        if (!this.props.isResizing && this.state.isResizing) {
            let width = this.containerRef.current.getBoundingClientRect().width;
            let difference =
                this.state.gameOffset +
                this.state.width +
                215 +
                this.rightOffset -
                width;
            let leftRightOffset = this.state.gameOffset + this.rightOffset;

            //Game banner fixed size of 200
            //Fixed padding between game and banner of 15
            //Minimum padding on left of game and right of banner of 10 each

            if (215 + this.state.width + leftRightOffset > width) {
                //Shrink game else shrink padding
                if (215 + this.state.width + 20 > width) {
                    this.rightOffset = 10;

                    this.setState({
                        isResizing: false,
                        width: width - 235,
                        gameOffset: 10,
                        bannerOffset: width - 210
                    });
                } else {
                    width = this.state.width;
                    this.rightOffset = (leftRightOffset - difference) / 2;

                    this.setState({
                        isResizing: false,
                        gameOffset: this.rightOffset,
                        bannerOffset: 15 + this.rightOffset + width
                    });
                }
            } else {
                //Enlarge game else enlarge padding
                if (this.state.width + 235 < 1040) {
                    let offset = 0;

                    if (width > 1040) {
                        width = 1040;
                        this.rightOffset = (leftRightOffset - difference) / 2;
                        if (this.rightOffset > 110) {
                            this.rightOffset = 110;
                        }
                        offset = this.rightOffset - 10;
                    }

                    this.setState({
                        isResizing: false,
                        width: width - 235,
                        gameOffset: this.rightOffset,
                        bannerOffset: offset + width - 210
                    });
                } else {
                    width = this.state.width;
                    this.rightOffset = (leftRightOffset - difference) / 2;

                    this.setState({
                        isResizing: false,
                        gameOffset: this.rightOffset,
                        bannerOffset: 15 + this.rightOffset + width
                    });
                }
            }

            this.isRedrawn = false;
        }

        if (
            !this.props.isResizing &&
            !this.state.isResizing &&
            !this.isRedrawn
        ) {
            this.isRedrawn = true;

            let context = this.canvasBack.current.getContext('2d');
            context.drawImage(
                this.background,
                0,
                0,
                this.state.width,
                this.state.height
            );

            this.drawCollectables();
            this.drawPlayer();
        }
    }
    componentDidMount() {
        resizeComponents();
        this.background.onload = () => {
            let context = this.canvasBack.current.getContext('2d');
            context.drawImage(
                this.background,
                0,
                0,
                this.state.width,
                this.state.height
            );
        };

        this.directionArrow.onload = () => {
            let context = this.arrowRef.current.getContext('2d');
            context.drawImage(this.directionArrow, 0, 0, 100, 100);
        };
        this.collectable.onload = this.drawCollectables;
        this.ship.onload = this.drawPlayer;

        //Need to change it here to have ignore threshold appear, which needed state to be mapNodes
        //Before it was added to state.
        this.setState({ mode: 'eyesMode' });
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('bciUpdate', this.bciUpdate);
            window.removeEventListener('keypress', this.handleKeypress);
        }
        clearInterval(this.gameRunInterval);
    }
}

BrainwaveGame.propTypes = {
    isResizing: PropTypes.bool,
    setOptionsCallback: PropTypes.func
};

BrainwaveGame.defaultProps = {
    isResizing: false
};

export default BrainwaveGame;
