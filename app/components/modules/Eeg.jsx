import React from 'react';
import PropTypes from 'prop-types';
import ModuleOptions from '../ModuleOptions';
import throttle from 'lodash.throttle';
import Config from '../ThreeD/Config.jsx';

class Eeg extends React.Component {
    constructor(props) {
        super(props);
        this.graphRefs = [];
        this.listRefs = [];
        this.containerRef = React.createRef();

        let channels = window.bciConfig.channels;
        let graphSetElements = [];
        let sensorElements = [];
        this.options = [];
        this.selectedCount = 0;
        this.restoreStlye = false;
        this.updateMaxWidth = false;
        this.scaleRanger = {};
        this.restoreScale = false;

        for (let i = 0; i < channels.length; i++) {
            let name = channels[i];
            this.graphRefs.push(React.createRef());
            this.listRefs.push(React.createRef());

            graphSetElements.push(
                <GraphSet
                    id={'graphSet' + name}
                    key={name}
                    channelIndex={i}
                    channelNames={[name]}
                    autoScale={false}
                    scale={20}
                    ref={this.graphRefs[i]}
                    onDragStart={this.onDragStart}
                    onDragOver={this.onDragOver}
                    onDrop={this.onDrop}
                    setSelected={this.setSelected}
                    updateSliders={this.updateSliders}
                />
            );

            sensorElements.push({
                id: name,
                type: 'selectable',
                text: 'Sensor ' + name,
                ref: this.listRefs[i],
                draggable: true,
                isselected: false,
                name: 'sensor' + name,
                onDragStart: this.onDragStart,
                onDragOver: this.onDragOver,
                onDrop: this.onDrop,
                onDrag: this.onDrag,
                onClick: this.setSelected
            });
        }

        // this.defaultCanvases = graphSetElements;
        // this.defaultList = sensorElements;

        if (this.props.setOptionsCallback) {
            this.props.setOptionsCallback(
                function() {
                    this.setState((prevState) => {
                        return {
                            optionsOpen: !prevState.optionsOpen
                        };
                    });
                }.bind(this)
            );
        }

        this.state = {
            canvases: graphSetElements,
            sensorList: sensorElements,
            timeWindow: 30,
            autoScale: false,
            scale: 20,
            width: 450,
            height: 400,
            url: '',
            isResizing: false,
            maxWidth: 1255,
            downloadState: {},
            optionsOpen: false,
            disableColours: false
        };
    }
    onDragStart = (e) => {
        this.held = e.target.id;
        this.heldAttribute = e.target.getAttribute('type');
    };
    onDrag = (e) => {
        e.preventDefault();
    };
    //On drop, reorder the list of graph sets and the corresponding elements
    onDrop = (e) => {
        e.preventDefault();
        let held = this.held;

        if (
            typeof held !== 'undefined' &&
            e.target.getAttribute('type') === this.heldAttribute
        ) {
            let target = e.target.id;
            if (target !== held) {
                let newState1 = [];
                let newState2 = [];
                let oldState1;
                let oldState2;

                let insertAfter = false;
                let heldIndex;

                oldState1 = this.state.canvases;
                oldState2 = this.state.sensorList;

                if (this.heldAttribute === 'graphSet') {
                    for (let i = 0; i < oldState1.length; i++) {
                        if ('graphSet' + oldState1[i].key === held) {
                            heldIndex = i;
                        }
                    }
                } else {
                    for (let i = 0; i < oldState1.length; i++) {
                        if (oldState1[i].key === held) {
                            heldIndex = i;
                        }
                    }
                }

                for (let i = 0; i < oldState1.length; i++) {
                    if ('graphSet' + oldState1[i].key === target) {
                        if (insertAfter === true) {
                            newState1.push(oldState1[i]);
                            newState1.push(oldState1[heldIndex]);
                            newState2.push(oldState2[i]);
                            newState2.push(oldState2[heldIndex]);
                        } else {
                            newState1.push(oldState1[heldIndex]);
                            newState1.push(oldState1[i]);
                            newState2.push(oldState2[heldIndex]);
                            newState2.push(oldState2[i]);
                        }
                    } else if (oldState1[i].key === oldState1[heldIndex].key) {
                        insertAfter = true;
                    } else {
                        newState1.push(oldState1[i]);
                        newState2.push(oldState2[i]);
                    }
                }

                this.setState({ canvases: newState1, sensorList: newState2 });
            }
        }
        this.held = undefined;
    };
    onDragOver = (e) => {
        e.preventDefault();
        e.stopPropagation();
    };
    invertSelection = () => {
        let sensors = this.state.sensorList;
        let selectedState;
        let canvases = this.state.canvases;

        if (this.selectedCount === 0) {
            selectedState = true;
        } else {
            selectedState = false;
        }

        this.selectedCount = 0;

        for (let i = 0; i < sensors.length; i++) {
            sensors[i].ref.current.isselected = !sensors[i].ref.current
                .isselected;
            if (sensors[i].ref.current.isselected) {
                sensors[i].ref.current.style.backgroundColor = '#E5E7E9';
                canvases[i].ref.current.canvas.style.borderColor = '#0066ff';
                this.selectedCount = sensors.length;
                this.selctedCount++;
            } else {
                sensors[i].ref.current.style.backgroundColor = 'white';
                canvases[i].ref.current.canvas.style.borderColor = 'black';
                this.selectedCount--;
            }
        }

        this.setState({ sensorList: sensors, selectState: selectedState });
    };
    //Set all sensors in options list as selected/unselected
    selectAll = () => {
        let sensors = this.state.sensorList;
        let selectedState;
        let canvases = this.state.canvases;

        if (this.selectedCount < sensors.length) {
            selectedState = true;
        } else {
            selectedState = false;
        }

        for (let i = 0; i < sensors.length; i++) {
            sensors[i].ref.current.isselected = selectedState;
            if (selectedState) {
                sensors[i].ref.current.style.backgroundColor = '#E5E7E9';
                canvases[i].ref.current.canvas.style.borderColor = '#0066ff';
                this.selectedCount = sensors.length;
            } else {
                sensors[i].ref.current.style.backgroundColor = 'white';
                canvases[i].ref.current.canvas.style.borderColor = 'black';
                this.selectedCount = 0;
            }
        }

        this.setState({ sensorList: sensors, selectState: selectedState });
    };
    //Set individual sensors in options list as selected/unselected
    setSelected = (e) => {
        let target = e.target.textContent || e.target.id;
        let sensors = this.state.sensorList;
        let canvases = this.state.canvases;
        let allSelected;

        for (let i = 0; i < sensors.length; i++) {
            if (
                target === 'Sensor ' + sensors[i].ref.current.id ||
                target === 'AvgOfSensors ' + sensors[i].ref.current.id ||
                target === 'graphSet' + sensors[i].ref.current.id
            ) {
                if (sensors[i].ref.current.isselected) {
                    sensors[i].ref.current.isselected = false;
                    sensors[i].ref.current.style.backgroundColor = 'white';
                    canvases[i].ref.current.canvas.style.borderColor = 'black';
                    this.selectedCount--;
                } else {
                    sensors[i].ref.current.isselected = true;
                    sensors[i].ref.current.style.backgroundColor = '#E5E7E9';
                    canvases[i].ref.current.canvas.style.borderColor =
                        '#0066ff';
                    this.selectedCount++;
                    this.setState({
                        width: canvases[i].ref.current.canvas.width,
                        height: canvases[i].ref.current.canvas.height
                    });
                }
            }
        }

        if (this.selectedCount < sensors.length) {
            allSelected = false;
        } else {
            allSelected = true;
        }

        this.setState({ sensorList: sensors, selectState: allSelected });
    };
    restoreDefaults = () => {
        this.restoreStyle = true;
        this.selectedCount = 0;
        this.setState({
            canvases: this.defaultCanvases,
            sensorList: this.defaultList,
            selectState: false,
            timeWindow: 30,
            downloadState: {},
            scale: 20,
            autoScale: false
        });
    };
    restoreStyles = () => {
        let canvases = this.state.canvases;
        let sensors = this.state.sensorList;
        for (let i = 0; i < canvases.length; i++) {
            canvases[i].ref.current.unmergeWaves();
            canvases[i].ref.current.updateDimensions(450, 400);
            sensors[i].ref.current.isselected = false;
            sensors[i].ref.current.style.backgroundColor = 'white';
            canvases[i].ref.current.canvas.style.borderColor = 'black';
            canvases[i].ref.current.updateColourState(false);
        }
        this.restoreStyle = false;
        this.setState({
            canvases: canvases,
            sensorList: sensors,
            disableColours: false
        });
    };
    setTimeWindow = () => {
        for (let i = 0; i < this.state.canvases.length; i++) {
            this.state.canvases[i].ref.current.updateTimeWindow(
                this.state.timeWindow
            );
        }
    };
    beginSave = () => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;

            let height = 0;
            let nextHeight = 0;
            let width = 0;
            let current;
            let images = [];

            let totalWidth = 0;
            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    totalWidth += canvases[i].ref.current.canvas.width;
                }
            }

            if (totalWidth > 1250) {
                totalWidth = 1250;
            }

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    current = canvases[i].ref.current.canvas;

                    if (width + current.width <= totalWidth) {
                        images.push({
                            canvas: current,
                            height: height,
                            width: width
                        });
                        width += current.width;
                        if (height + current.height > nextHeight) {
                            nextHeight = height + current.height;
                        }
                    } else {
                        height = nextHeight;
                        images.push({
                            canvas: current,
                            height: height,
                            width: 0
                        });
                        width = current.width;
                        nextHeight = height + current.height;
                    }
                }
            }

            this.images = images;
            this.saveRef = React.createRef();
            let newCanvas = (
                <canvas
                    ref={this.saveRef}
                    width={totalWidth}
                    height={nextHeight}
                />
            );
            this.saving = true;
            this.setState({ saveCanvas: newCanvas });
        }
    };
    finaliseSave = () => {
        let context = this.state.saveCanvas.ref.current.getContext('2d');

        for (let i = 0; i < this.images.length; i++) {
            context.drawImage(
                this.images[i].canvas,
                this.images[i].width,
                this.images[i].height
            );
        }

        let dataURL = this.state.saveCanvas.ref.current.toDataURL('image/png');

        this.saving = false;

        let downloadState = {
            type: 'downloadbutton',
            text: 'Begin Download',
            name: 'downloadButton',
            href: dataURL,
            download: 'Brain Waves'
        };

        this.setState({ saveCanvas: '', downloadState: downloadState });
    };
    mergeGraphSets = () => {
        if (this.selectedCount > 1) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;
            let newCanvases = [];
            let newSensors = [];
            let names = [];
            let isSelected = false;
            let pastNames;
            let name = '';
            let scale = this.state.scale;

            if(this.state.autoScale){
                scale = 0;
            }

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    pastNames = canvases[i].ref.current.channelNames;

                    for (let j = 0; j < pastNames.length; j++) {
                        names.push(pastNames[j]);
                        name += pastNames[j];
                    }
                    this.selectedCount--;
                    isSelected = true;
                }
            }

            if (isSelected) {
                this.graphRefs.push(React.createRef());
                this.listRefs.push(React.createRef());

                newCanvases.push(
                    <GraphSet
                        id={'graphSet' + names.join(', ')}
                        key={name}
                        channelNames={names}
                        ref={this.graphRefs[this.graphRefs.length - 1]}
                        onDragStart={this.onDragStart}
                        onDragOver={this.onDragOver}
                        onDrop={this.onDrop}
                        setSelected={this.setSelected}
                        updateSliders={this.updateSliders}
                        autoScale= {this.state.autoScale}
                        scale={scale}
                    />
                );

                newSensors.push({
                    id: names.join(', '),
                    type: 'selectable',
                    text: 'AvgOfSensors ' + names.join(', '),
                    ref: this.listRefs[this.listRefs.length - 1],
                    draggable: true,
                    isselected: false,
                    name: 'sensor' + name,
                    onDragStart: this.onDragStart,
                    onDragOver: this.onDragOver,
                    onDrop: this.onDrop,
                    onDrag: this.onDrag,
                    onClick: this.setSelected
                });

                for (let i = 0; i < canvases.length; i++) {
                    if (!sensors[i].ref.current.isselected) {
                        newCanvases.push(canvases[i]);
                        newSensors.push(sensors[i]);
                    }
                }

                this.selctedCount = 0;
                this.setState({
                    canvases: newCanvases,
                    sensorList: newSensors,
                    selectState: false
                });
            }
        }
    };
    unmergeGraphSets = () => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;
            let defaultList = this.defaultList;
            let defaults = this.defaultCanvases;
            let names;
            let newCanvases = [];
            let newSensors = [];
            let isSelected = false;

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    names = canvases[i].ref.current.channelNames;

                    if (names.length > 1) {
                        isSelected = true;

                        for (let j = 0; j < names.length; j++) {
                            for (let k = 0; k < defaults.length; k++) {
                                if (names[j] === defaults[k].key) {
                                    newCanvases.push(defaults[k]);
                                    newSensors.push(defaultList[k]);
                                }
                            }
                        }
                    }
                    sensors[i].ref.current.isselected = false;
                    sensors[i].ref.current.style.backgroundColor = 'white';
                    this.selectedCount--;
                }

                if (isSelected === false) {
                    newCanvases.push(canvases[i]);
                    newSensors.push(sensors[i]);
                }

                isSelected = false;
            }
            this.restoreScale = true;

            this.setState({
                canvases: newCanvases,
                sensorList: newSensors,
                selectState: false
            });
        }
    };
    mergeWaves = () => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    canvases[i].ref.current.mergeWaves();
                }
            }
        }
    };
    unmergeWaves = () => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    canvases[i].ref.current.unmergeWaves();
                }
            }
        }
    };
    updateSliders = (width, height) => {
        this.setState({ width: width, height: height });
    };
    updateWidth = (e) => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;
            let width = e.target.value;
            let height = this.state.height;

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    canvases[i].ref.current.updateDimensions(width, height);
                }
            }
            this.setState({ width: width });
        }
    };
    updateHeight = (e) => {
        if (this.selectedCount > 0) {
            let canvases = this.state.canvases;
            let sensors = this.state.sensorList;
            let width = this.state.width;
            let height = e.target.value;

            for (let i = 0; i < canvases.length; i++) {
                if (sensors[i].ref.current.isselected) {
                    canvases[i].ref.current.updateDimensions(width, height);
                }
            }

            this.setState({ height: height });
        }
    };
    disableColours = () => {
        let state = !this.state.disableColours;
        let canvases = this.state.canvases;

        for (let i = 0; i < canvases.length; i++) {
            canvases[i].ref.current.updateColourState(state);
        }

        this.setState({ disableColours: state });
    };
    setScale = (e) => {
        let canvases = this.state.canvases;

        for (let i = 0; i < canvases.length; i++) {
            if (canvases[i].ref.current.scale !== e.target.value) {
                canvases[i].ref.current.updateScale(e.target.value);
            }
        }

        this.setState({ scale: e.target.value });
    };
    updateScaleMode = () => {
        let state = !this.state.autoScale;
        let canvases = this.state.canvases;

        for (let i = 0; i < canvases.length; i++) {
            canvases[i].ref.current.updateScaleMode(state);
        }

        if (!state) {
            for (let i = 0; i < canvases.length; i++) {
                canvases[i].ref.current.updateScale(20);
            }
            this.setState({
                scale: 20,
                autoScale: state
            });
        } else {
            this.scaleRanger = {};
            this.setState({
                autoScale: state
            });
        }
    };
    restoreScaleValues = () => {
        let canvases = this.state.canvases;

        for (let i = 0; i < canvases.length; i++) {
            canvases[i].ref.current.updateScale(this.state.scale);
            canvases[i].ref.current.updateScaleMode(this.state.autoScale);
            
        }
    }
    getOptions = () => {
        if (!this.state.autoScale) {
            this.scaleRanger = {
                type: 'ranger',
                text: 'Scale',
                desc: 'Maximum Value: ' + this.state.scale,
                name: 'scale',
                min: 1,
                max: 250,
                value: this.state.scale,
                onChange: this.setScale
            };
        }

        return [
            {
                type: 'submenu',
                text: 'Sensors',
                desc: 'The list of sensors',
                options: [
                    {
                        type: 'text',
                        text: 'Merge/Unmerge affects selected sensors'
                    },
                    {
                        type: 'button',
                        name: 'mergeSensorsButton',
                        text: 'Merge Sensors',
                        onClick: this.mergeGraphSets
                    },
                    {
                        type: 'button',
                        name: 'unmergeSensorsButton',
                        text: 'Unmerge Sensors',
                        onClick: this.unmergeGraphSets
                    },
                    {
                        type: 'br'
                    },
                    {
                        type: 'button',
                        name: 'mergeWavesButton',
                        text: 'Merge Waves',
                        onClick: this.mergeWaves
                    },
                    {
                        type: 'button',
                        name: 'unmergeWavesButton',
                        text: 'Unmerge Waves',
                        onClick: this.unmergeWaves
                    },
                    {
                        type: 'br'
                    },
                    {
                        type: 'button',
                        name: 'invertSelection',
                        text: 'Invert Selection',
                        onClick: this.invertSelection
                    },
                    {
                        type: 'checkbox',
                        text: 'Select All',
                        name: 'selectState',
                        value: this.state.selectState,
                        onChange: this.selectAll
                    },
                    {
                        type: 'list',
                        list: this.state.sensorList
                    }
                ]
            },
            {
                type: 'button',
                name: 'restoreButton',
                text: 'Restore defaults',
                onClick: this.restoreDefaults
            },
            {
                type: 'br'
            },
            {
                type: 'button',
                name: 'saveMode',
                text: 'Save Selected Sensors',
                onClick: this.beginSave
            },

            this.state.downloadState,

            {
                type: 'checkbox',
                text: 'Disable Colours',
                name: 'disableColours',
                value: this.state.disableColours,
                onChange: this.disableColours
            },
            {
                type: 'checkbox',
                text: 'Automatic Scale',
                name: 'autoScale',
                value: this.state.autoScale,
                onChange: this.updateScaleMode
            },
            this.scaleRanger,
            {
                type: 'ranger',
                text: 'Time Window',
                desc: 'Time Window Size (s): ' + this.state.timeWindow,
                name: 'timeWindow',
                min: 0,
                max: 60,
                value: this.state.timeWindow
            },
            {
                type: 'ranger',
                text: 'Width',
                desc: 'Change width of selected graph sets',
                name: 'width',
                min: 137,
                max: this.state.maxWidth,
                value: this.state.width,
                onChange: this.updateWidth
            },
            {
                type: 'ranger',
                text: 'Height',
                desc: 'Change height of selected graph sets',
                name: 'height',
                min: 165,
                max: 540,
                value: this.state.height,
                onChange: this.updateHeight
            }
        ];
    };
    optionsCallback = (newState) => {
        this.setState(newState);
    };
    render() {
        return (
            <div
                onDragOver={(e) => e.preventDefault()}
                onDrop={(e) => e.preventDefault()}
                ref={this.containerRef}
            >
                <div id="EEG_CHART">
                    {this.state.canvases}
                    {this.state.saveCanvas}
                </div>
                <ModuleOptions
                    open={this.state.optionsOpen}
                    options={this.getOptions()}
                    optionsCallback={this.optionsCallback}
                />
            </div>
        );
    }
    componentDidUpdate() {
        //Start resizing
        if (this.props.isResizing) {
            let width = Math.floor(
                this.containerRef.current.getBoundingClientRect().width - 23
            );
            for (let i = 0; i < this.state.canvases.length; i++) {
                this.state.canvases[i].ref.current.updateMaxWidth(width);
            }
            this.updateMaxWidth = true;
        }

        if (this.updateMaxWidth && !this.props.isResizing) {
            this.updateMaxWidth = false;
            this.setState({
                maxWidth:
                    this.containerRef.current.getBoundingClientRect().width - 23
            });

            let width = Math.floor(
                this.containerRef.current.getBoundingClientRect().width - 23
            );
            for (let i = 0; i < this.state.canvases.length; i++) {
                this.state.canvases[i].ref.current.updateMaxWidth(width);
            }
        }

        if (this.saving) {
            this.finaliseSave();
        }
        if (this.restoreStyle) {
            this.restoreStyles();
        }
        if(this.restoreScale){
            this.restoreScale = false;
            this.restoreScaleValues();
        }
        this.setTimeWindow();
    }
    componentDidMount() {
        this.defaultCanvases = this.state.canvases;
        this.defaultList = this.state.sensorList;
    }
}

class GraphSet extends React.Component {
    constructor(props) {
        super(props);
        this.channelNames = this.props.channelNames;
        this.timeWindow = 30;
        this.mergeWaveGraphs = false;
        this.resizingHorizontal = false;
        this.resizingVertical = false;
        this.maxWidth = 1255;
        this.minWidth = 165;
        this.disableColours = false;
        this.autoScale = this.props.autoScale;
        this.resetCount = 0;

        this.colours = {};

        let colours = [
            Config.thetaColor,
            Config.alphaColorEEG,
            Config.lowBetaColor,
            Config.highBetaColorEEG,
            Config.gammaColor
        ];

        for (let i = 0; i < window.bciConfig.frequencies.length; i++) {
            this.colours['' + window.bciConfig.frequencies[i]] = colours[i];
        }

        if (this.channelNames.length > 1) {
            this.minWidth = 211;
        }

        let frequencies = window.bciConfig.frequencies;

        this.yMax = new Array(frequencies.length + 1).fill().map(() => this.props.scale);
        this.averages = new Array(frequencies.length + 1).fill().map(() => 0);

        let waves = new Array(frequencies.length + 1)
            .fill()
            .map(() => ({ values: [] }));
        let topPadding = 40;

        if (this.channelNames.length > 5) {
            topPadding = 60;
        }

        let graphHeight =
            (400 - topPadding - 15 * frequencies.length) / frequencies.length;

        this.dimensions = {
            leftPadding: 18,
            rightPadding: 65,
            topPadding: topPadding,
            innerPadding: 15,
            graphHeight: graphHeight,
            graphWidth: 367
        };

        this.graphData = {
            waves,
            length: 0
        };

        this.state = {
            width: 450,
            height: 400
        };

        // Register a function to receive the global bciUpdate events
        // have a look in App.jsx for the structure of this event
        this.onMouseMoveThrottled = throttle(this.onMouseMove, 50);
        if (typeof window !== 'undefined') {
            window.addEventListener('bciUpdate', this.bciUpdate);
            window.addEventListener('mouseup', this.onMouseUp);
            window.addEventListener('mousemove', this.onMouseMoveThrottled);
            window.addEventListener('mousedown', this.onDragStart);
        }
    }
    //Updates the data of this graph set then draws it.
    bciUpdate = (e) => {
        let sensorNames = this.channelNames;
        let value = 0;
        if (this.graphData.length < 100000) {
            let frequencies = window.bciConfig.frequencies;

            for (let j = 0; j < frequencies.length; j++) {
                value = 0;
                for (let i = 0; i < sensorNames.length; i++) {
                    value += e.detail.channels[sensorNames[i]][frequencies[j]];
                }

                value /= sensorNames.length;

                if(this.autoScale){
                     if (value < this.averages[j] * 8 || this.averages[j] === 0
                    ) {
                        this.averages[j] = (this.averages[j] + value) / 2;
                    }

                    if (value > this.yMax[j] &&
                        value < this.averages[j] * 16
                    ) {
                        this.yMax[j] = value;
                        this.resetCount = 0;
                    }else if(value > this.averages[j] * 16){
                        this.resetCount++;
                        if(this.resetCount === 30){
                            this.averages[j] = 0;
                        }
                    }
                }
               

                this.graphData.waves[j].values.push({
                    X: e.detail.interval / 1000,
                    Y: value
                });
            }

            value = 0;

            for (let j = 0; j < frequencies.length; j++) {
                value += this.graphData.waves[j].values[this.graphData.length]
                    .Y;
            }
            value /= frequencies.length;

            if (
                this.autoScale &&
                (this.value < this.averages[frequencies.length] * 2 ||
                    this.averages[frequencies.length] === 0)
            ) {
                this.averages[frequencies.length] =
                    (this.averages[frequencies.length] + value) / 2;
            }

            if (
                this.autoScale &&
                value > this.yMax[frequencies.length] &&
                value < this.averages[frequencies.length] * 4
            ) {
                this.yMax[frequencies.length] = value;
            }

            this.graphData.waves[frequencies.length].values.push({
                X: e.detail.interval / 1000,
                Y: value
            });

            this.graphData.length += 1;
        }

        //Need to stop drawing while resizing to prevent graphical bug.
        if (!this.resizingHorizontal && !this.resizingVertical) {
            this.drawLines();
        }
    };
    //Draws the graph backgrounds and text for the graph set
    loadBackground() {
        let graphSetWidth = this.state.width;
        let graphSetHeight = this.state.height;
        let context = this.canvas.getContext('2d');
        let dimensions = this.dimensions;
        let frequencies = window.bciConfig.frequencies;
        let titles = this.titles;

        if (this.mergeWaveGraphs) {
            frequencies = [''];
        }

        context.clearRect(0, 0, graphSetWidth, graphSetHeight);
        context.stroke();

        context.lineWidth = 1;
        context.strokeStyle = '#333';

        // //Titles
        context.font = 'bold 12pt sans-serif';
        context.textAlign = 'center';

        for (let i = 0; i < titles.length; i++) {
            context.fillText(
                titles[i],
                dimensions.innerPadding + dimensions.graphWidth / 2 + 25, // 25 is just some offset that works
                24 * (i + 1)
            );
            context.stroke();
        }

        context.font = 'bold 10pt sans-serif';
        context.textAlign = 'left';

        // Frequency names.
        for (let i = 0; i < frequencies.length; i++) {
            context.fillText(
                frequencies[i],
                graphSetWidth - dimensions.rightPadding + 6,
                dimensions.topPadding +
                    dimensions.graphHeight / 2 +
                    (dimensions.innerPadding + dimensions.graphHeight) * i
            );
        }
        context.stroke();

        //canvas outline
        context.rect(0, 0, graphSetWidth, graphSetHeight);
        context.stroke();

        //graph backgrounds
        context.fillStyle = '#E5E7E9';
        context.beginPath();

        for (let i = 0; i < frequencies.length; i++) {
            context.fillRect(
                dimensions.leftPadding,
                dimensions.topPadding +
                    (dimensions.graphHeight + dimensions.innerPadding) * i,
                dimensions.graphWidth,
                dimensions.graphHeight
            );
        }
        context.stroke();

        //graph borders
        context.lineWidth = 2;
        context.fillStyle = '#333';
        context.beginPath();

        for (let i = 0; i < frequencies.length; i++) {
            context.rect(
                dimensions.leftPadding,
                dimensions.topPadding +
                    (dimensions.graphHeight + dimensions.innerPadding) * i,
                dimensions.graphWidth,
                dimensions.graphHeight
            );
        }
        context.stroke();
    }
    //draws the data as a line graph from right to left
    drawLines() {
        let graphSetWidth = this.state.width;
        let context = this.canvas.getContext('2d');
        let frequencies = window.bciConfig.frequencies;
        let frequenciesLength = frequencies.length;
        let dimensions = this.dimensions;
        let data = this.graphData;
        let time = 0;
        let timeWindow = this.timeWindow;
        let xPos = graphSetWidth - dimensions.rightPadding;
        let index, timeChange, yPercent;

        if (this.mergeWaveGraphs) {
            frequencies = [''];
        }

        context.fillStyle = '#E5E7E9';

        //clear previously drawn lines
        //redraw graph background and borders to 'clear' the previously drawn lines
        for (let i = 0; i < frequencies.length; i++) {
            context.fillRect(
                dimensions.leftPadding,
                dimensions.topPadding +
                    (dimensions.graphHeight + dimensions.innerPadding) * i,
                dimensions.graphWidth,
                dimensions.graphHeight
            );
            context.rect(
                dimensions.leftPadding,
                dimensions.topPadding +
                    (dimensions.graphHeight + dimensions.innerPadding) * i,
                dimensions.graphWidth,
                dimensions.graphHeight
            );
        }
        context.stroke();
        //Begin Drawing graph
        context.lineWidth = 1;
        context.strokeStyle = '#333';
        context.lineCap = 'round';

        for (let j = 0; j < frequencies.length; j++) {
            if (!this.disableColours) {
                context.strokeStyle =
                    'rgb(' + this.colours[frequencies[j]] + ')';
            }

            let k = j;
            context.beginPath();
            index = data.length - 1;
            time = 0;

            if (this.mergeWaveGraphs) {
                j = frequenciesLength;
            }

            yPercent = 1 - data.waves[j].values[index].Y / this.yMax[j];

            if (yPercent < 0) {
                yPercent = 0;
            }

            context.moveTo(
                xPos,
                dimensions.topPadding +
                    yPercent * dimensions.graphHeight +
                    (dimensions.graphHeight + dimensions.innerPadding) * k
            );

            for (
                ;
                index > -1 &&
                time + data.waves[j].values[index].X <= timeWindow;
                index--
            ) {
                time += data.waves[j].values[index].X;
                timeChange = (timeWindow - time) / timeWindow;
                yPercent = 1 - data.waves[j].values[index].Y / this.yMax[j];

                if (yPercent < 0) {
                    yPercent = 0;
                }

                context.lineTo(
                    dimensions.leftPadding + dimensions.graphWidth * timeChange,
                    dimensions.topPadding +
                        yPercent * dimensions.graphHeight +
                        (dimensions.graphHeight + dimensions.innerPadding) * k
                );
            }
            //makes sure the last line drawn is not drawn beyond the boundary
            if (
                index > -1 &&
                time + data.waves[j].values[index].X > timeWindow
            ) {
                yPercent = 1 - data.waves[j].values[index].Y / this.yMax[j];

                if (yPercent < 0) {
                    yPercent = 0;
                }
                context.lineTo(
                    dimensions.leftPadding,
                    dimensions.topPadding +
                        yPercent * dimensions.graphHeight +
                        (dimensions.graphHeight + dimensions.innerPadding) * k
                );
            }

            context.stroke();

            context.beginPath();
            context.strokeStyle = '#333';
            context.rect(
                dimensions.leftPadding,
                dimensions.topPadding +
                    (dimensions.graphHeight + dimensions.innerPadding) * j,
                dimensions.graphWidth,
                dimensions.graphHeight
            );
            context.stroke();
        }
    }
    //Check if the user is attempting to resize or move the canvas
    onDragStart = (e) => {
        if (e.target.id === this.props.id) {
            let bounds = this.canvas.getBoundingClientRect();
            let x = e.clientX - bounds.left;
            let y = e.clientY - bounds.top;

            this.targetid = e.target.id;

            if (x > this.state.width - 10) {
                this.resizingHorizontal = true;
                this.initialX = x;
            }

            if (y > bounds.height - 10) {
                this.resizingVertical = true;
                this.initialY = y;
            }

            if (!this.resizingHorizontal && !this.resizingVertical) {
                this.props.onDragStart(e);
            } else {
                e.preventDefault();
            }
        }
    };
    onMouseMove = (e) => {
        let bounds = this.canvas.getBoundingClientRect();
        let x = e.clientX - bounds.left;
        let y = e.clientY - bounds.top;

        if (e.target.id === this.props.id) {
            if (x > this.state.width - 10 && y > bounds.height - 10) {
                this.canvas.style.cursor = 'nwse-resize';
            } else if (x > this.state.width - 10) {
                this.canvas.style.cursor = 'ew-resize';
            } else if (y > bounds.height - 10) {
                this.canvas.style.cursor = 'ns-resize';
            } else {
                this.canvas.style.cursor = 'default';
            }
        }

        //resize canvas if it is the canvas to resize
        if (
            (this.resizingHorizontal || this.resizingVertical) &&
            this.targetid !== undefined
        ) {
            e.preventDefault();
            let newWidth = this.state.width;
            let newHeight = this.state.height;

            if (this.resizingHorizontal) {
                newWidth = newWidth + x - this.initialX;
            }

            if (this.resizingVertical) {
                newHeight = newHeight + y - this.initialY;
            }

            this.initialX = x;
            this.initialY = y;

            //Check that  than 0 for is the user ends the drag outside the window
            if (newWidth > 0) {
                if (newWidth < this.minWidth) {
                    newWidth = this.minWidth;
                } else if (newWidth > this.maxWidth) {
                    newWidth = this.maxWidth;
                }

                if (newHeight < 233 && !this.mergeWaveGraphs) {
                    newHeight = 233;
                } else if (newHeight < 79 && this.mergeWaveGraphs) {
                    newHeight = 79;
                    frequencies = 1;
                } else if (newHeight > 540) {
                    newHeight = 540;
                }

                this.updateDimensions(newWidth, newHeight);
                this.props.updateSliders(newWidth, newHeight);
            }
        }
    };
    onMouseUp = (e) => {
        if (e.target.id === this.props.id) {
            if (!this.resizingHorizontal && !this.resizingVertical) {
                this.props.setSelected(e);
            }
        }

        this.resizingHorizontal = false;
        this.resizingVertical = false;
        this.targetid = undefined;
    };
    updateTimeWindow(value) {
        this.timeWindow = value;
    }

    mergeWaves = () => {
        if (!this.mergeWaveGraphs) {
            this.mergeWaveGraphs = true;
            this.dimensions.rightPadding = 18;

            this.updateDimensions(
                this.state.width,
                (this.state.height - this.dimensions.topPadding) / 5 +
                    this.dimensions.topPadding
            );
        }
    };
    unmergeWaves = () => {
        if (this.mergeWaveGraphs) {
            this.mergeWaveGraphs = false;
            this.dimensions.rightPadding = 65;

            this.updateDimensions(
                this.state.width,
                (this.state.height - this.dimensions.topPadding) * 5 +
                    this.dimensions.topPadding
            );
        }
    };
    updateMaxWidth = (width) => {
        this.maxWidth = width;
        if (this.state.width > width && width > this.minWidth) {
            this.updateDimensions(width, this.state.height);
        }
    };
    updateColourState = (state) => {
        this.disableColours = state;
    };
    updateScaleMode = (state) => {
        if (state) {
            for (let i = 0; i < this.yMax.length; i++) {
                this.yMax[i] = 0;
                this.averages[i] = 0;
            }
        }

        this.autoScale = state;
    };
    updateScale = (yMax) => {
        for (let i = 0; i < this.yMax.length; i++) {
            this.yMax[i] = yMax;
        }
    };
    setTitles = () => {
        let context = this.canvas.getContext('2d');
        let title;
        this.titles = [];

        context.font = 'bold 12pt sans-serif';

        if (this.channelNames.length === 1) {
            this.titles.push('Sensor ' + this.channelNames[0]);
        } else {
            title = 'Average of Sensors';
            let width =
                this.dimensions.graphWidth > this.minWidth
                    ? this.dimensions.graphWidth
                    : this.state.width;

            for (let i = 0; i < this.channelNames.length - 1; i++) {
                if (
                    context.measureText(
                        title + ' ' + this.channelNames[i] + ','
                    ).width <= width
                ) {
                    title += ' ' + this.channelNames[i] + ',';
                } else {
                    this.titles.push(title);
                    title = this.channelNames[i];

                    if (this.channelNames.length - i > 1) {
                        title += ',';
                    }
                }
            }

            if (
                context.measureText(
                    title +
                        ' ' +
                        this.channelNames[this.channelNames.length - 1]
                ).width <= width
            ) {
                title += ' ' + this.channelNames[this.channelNames.length - 1];
                this.titles.push(title);
            } else {
                this.titles.push(title);
                this.titles.push(
                    this.channelNames[this.channelNames.length - 1]
                );
            }
        }
    };
    updateDimensions = (width, height) => {
        let frequencies = window.bciConfig.frequencies.length;

        if (width < this.minWidth) {
            width = this.minWidth;
        }

        if (this.mergeWaveGraphs) {
            frequencies = 1;
        }

        this.setTitles();

        this.dimensions.topPadding = this.titles.length * 20 + 20;

        let graphHeight =
            (height -
                this.dimensions.topPadding -
                this.dimensions.innerPadding * frequencies) /
            frequencies;
        let graphWidth =
            width - this.dimensions.leftPadding - this.dimensions.rightPadding;

        this.dimensions.graphWidth = graphWidth;
        this.dimensions.graphHeight = graphHeight;

        this.setState({ width: width, height: height });
        this.loadBackground();
    };
    render() {
        return (
            <canvas
                id={this.props.id}
                ref={(ref) => (this.canvas = ref)}
                className="chartBordersEEG"
                width={this.state.width}
                height={this.state.height}
                draggable="true"
                type="graphSet"
                onDragStart={this.onDragStart}
                onDrop={this.props.onDrop}
            />
        );
    }

    componentDidMount() {
        this.setTitles();
        this.loadBackground();
    }
    componentDidUpdate() {
        this.loadBackground();
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('bciUpdate', this.bciUpdate);
            window.removeEventListener('mouseup', this.onMouseUp);
            window.removeEventListener('mousemove', this.onMouseMoveThrottled);
            window.removeEventListener('mousedown', this.onDragStart);
        }
    }
}

Eeg.propTypes = {
    isResizing: PropTypes.bool,
    setOptionsCallback: PropTypes.func
};

Eeg.defaultProps = {
    isResizing: false
};

GraphSet.propTypes = {
    id: PropTypes.string,
    channelNames: PropTypes.array,
    onDragStart: PropTypes.func,
    onDrop: PropTypes.func,
    setSelected: PropTypes.func,
    updateSliders: PropTypes.func,
    autoScale: PropTypes.bool,
    scale: PropTypes.number
};

GraphSet.defaultProps = {};

export default Eeg;
