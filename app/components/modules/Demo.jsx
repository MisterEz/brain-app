import React from 'react';
import PropTypes from 'prop-types';
import ModuleOptions from '../ModuleOptions';
import Modal from '../Modal';

/* Demo:
 * This might become the defacto styleguide
 */
class Demo extends React.Component {
    constructor(props) {
        super(props);

        // For using options
        // values must be from the state
        this.state = {
            testCheckbox: false,
            testCheckbox2: true,
            testCheckbox3: false,
            ranger1: 15,
            ranger2: 50,
            ranger3: 2,
            mode: 'modeBtn1',
            modalOpen: false
        };

        //Clone the state for later
        this.defaultState = Object.assign({}, this.state);

        if (this.props.setOptionsCallback) {
            this.props.setOptionsCallback(
                function() {
                    this.setState((prevState) => {
                        return {
                            optionsOpen: !prevState.optionsOpen
                        };
                    });
                }.bind(this)
            );
        }
    }
    handleReset = () => {
        this.setState(this.defaultState);
    };
    optionsCallback = (newState) => {
        this.setState(newState);
    };
    openModal = () => {
        this.setState({ modalOpen: true });
    };
    closeModal = () => {
        this.setState({ modalOpen: false });
    };
    getOptions = () => {
        // This is a big example
        // Its only its own function to help organise things
        // as long as its updated at render time its okey-dokey
        return [
            {
                type: 'checkbox', //Text [X]
                text: 'Test',
                desc: 'this is a toggle type deal',
                name: 'testCheckbox', // this _must_ be the key name in state
                value: this.state.testCheckbox // this _must_ match the previous name
            }, // value:this.state.{{{testCheckbox}}} name:{{{testCheckbox}}}
            {
                type: 'checkbox',
                text: 'Test2',
                desc: 'the second of all time',
                name: 'testCheckbox2',
                value: this.state.testCheckbox2
            },
            {
                type: 'checkbox',
                text: 'Test3',
                desc: 'el testo the third',
                name: 'testCheckbox3',
                value: this.state.testCheckbox3
            },
            {
                type: 'submenu',
                text: 'Rangers',
                desc: 'Sliders, rangers, slidey things',
                options: [
                    {
                        type: 'ranger',
                        text: 'Ranger1',
                        desc: 'slido this sido',
                        name: 'ranger1',
                        value: this.state.ranger1
                    },
                    {
                        type: 'ranger',
                        text: 'Ranger2',
                        desc: 'don\t slide this on unless you are ready',
                        name: 'ranger2',
                        value: this.state.ranger2
                    },
                    {
                        type: 'ranger',
                        text: 'Ranger3',
                        desc: '1-5 feel alive',
                        name: 'ranger3',
                        min: 0,
                        max: 5,
                        value: this.state.ranger3
                    },
                    {
                        type: 'submenu',
                        text: 'Go Deeper',
                        desc: 'If you dare',
                        options: [
                            {
                                type: 'submenu',
                                text: 'Go even deeper',
                                desc: 'I double dare you',
                                options: [
                                    {
                                        type: 'text',
                                        text: 'Go away',
                                        name: 'ranger1'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                type: 'heading',
                text: 'Buttons'
            },
            {
                type: 'text',
                text: 'Bootstrap buttons 🙈'
            },
            {
                type: 'buttonGroup',
                text: 'Mode',
                desc: 'Assign the mode of operations',
                name: 'mode',
                value: this.state.mode, //Remember must be the same as 'name'
                btns: [
                    {
                        type: 'button',
                        name: 'modeBtn1', //This will become the new mode name
                        text: 'Mode1'
                    },
                    {
                        type: 'button',
                        name: 'modeBtn2',
                        text: 'Mode2'
                    },
                    {
                        type: 'button',
                        name: 'modeBtn3',
                        text: 'Mode3'
                    }
                ]
            },
            {
                type: 'text',
                text: 'Stateless buttons (they run a function)'
            },
            {
                type: 'button',
                className: 'btn-primary', //Add button classes here. 'btn-default' is default https://getbootstrap.com/docs/3.3/css/#buttons
                name: 'startBtn', //Buttons don't directly align with state so this is just used as an ID
                text: 'Start', //Text inside the button
                onClick: () => {
                    window.alert("Vrrrrm vroom vrooooom i'm a car");
                } //don't do this
            },
            {
                type: 'button',
                name: 'resetBtn',
                text: 'Reset',
                onClick: this.handleReset //callback to bound function
            },
            {
                type: 'hr'
            },
            {
                type: 'button',
                className: 'btn-block btn-danger',
                name: 'selfDestructBtn',
                text: 'DO NOT PRESS',
                onClick: () => {
                    document.getElementById('root').innerHTML =
                        '<h1 style="text-align: center">boom</h1>';
                }
            }
        ];
    };
    render() {
        /* This is the alternative way to declare options

        let options = [
            {
                type: 'heading',
                text: 'Options'
            },
            {
                type: 'button',
                name: 'resetBtn',
                text: 'Reset',
                onClick: this.handleReset //callback to bound function
            },
        ];

        // then in render
        // <ModuleOptions options={options} optionsCallback={this.optionsCallback}/>
        */

        let modal = null;
        if (this.state.modalOpen) {
            modal = (
                <Modal
                    onClose={this.closeModal}
                    header={<h2>Hi! I'm a modal</h2>}
                    body={
                        <div className="alert alert-info">
                            <p>
                                <b>Hey</b> I'm also the baller from gawler.
                            </p>
                        </div>
                    }
                    footer={
                        <div className="text-center">
                            <button
                                type="button"
                                className="btn btn-primary btn-block"
                                onClick={this.closeModal}
                            >
                                I've seen enough
                            </button>
                            <br />
                            <a onClick={this.closeModal} href="#">
                                I could have seen more
                            </a>
                        </div>
                    }
                    closeable={true}
                />
            );
        }

        return (
            <div className="Demo">
                {modal}
                {/* https://getbootstrap.com/docs/3.3/css/#grid */}
                <div className="row">
                    <div
                        className={['col-sm-4']
                            .concat(
                                this.state.testCheckbox
                                    ? ['bg-success']
                                    : ['bg-danger']
                            )
                            .join(' ')}
                    >
                        <h3>Test1</h3>
                        <div style={{ height: this.state.ranger1 }} />
                        <button
                            className="btn btn-primary"
                            onClick={this.openModal}
                        >
                            Click Me!
                        </button>
                    </div>
                    <div
                        className={['col-sm-4']
                            .concat(
                                this.state.testCheckbox2
                                    ? ['bg-success']
                                    : ['bg-danger']
                            )
                            .join(' ')}
                    >
                        <h3>Test2</h3>
                        <div style={{ height: this.state.ranger2 }} />
                        Mode: {this.state.mode}
                    </div>
                    <div
                        className={['col-sm-4']
                            .concat(
                                this.state.testCheckbox3
                                    ? ['bg-success']
                                    : ['bg-danger']
                            )
                            .join(' ')}
                    >
                        <h3>Test3</h3>
                        <div style={{ height: this.state.ranger3 * 10 }} />
                    </div>
                </div>
                <ModuleOptions
                    open={this.state.optionsOpen}
                    options={this.getOptions()}
                    optionsCallback={this.optionsCallback}
                />
            </div>
        );
    }
}

export default Demo;

Demo.propTypes = {
    setOptionsCallback: PropTypes.func
};
