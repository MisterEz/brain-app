import React from 'react';
import Battery from '../Status/Battery';
import Connection from '../Status/Connection';
import SensorQuality from '../Status/SensorQuality';

class Status extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            updateHz: 0
        };

        if (typeof window !== 'undefined') {
            window.addEventListener('bciUpdate', this.bciUpdate);
        }
        this.avgInterval = 100;
        this.noUpdateCount = 10;
    }
    // This function will get called when there is new status info from the headset
    bciUpdate = (e) => {
        this.avgInterval = (this.avgInterval * 9 + e.detail.interval) / 10;
        this.noUpdateCount++;
        if (this.noUpdateCount > 10) {
            this.setState({
                updateHz: Math.round(1000 / this.avgInterval)
            });
            this.noUpdateCount = 0;
        }
    };
    render() {
        // Components are rendered once and then again everytime the state changes
        return (
            <div className="Status">
                <div className="row">
                    <div className="col-xs-12">
                        <div className="alert alert-danger noHeadsetOnly">
                            <strong>No EEG device detected</strong>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <SensorQuality size={1} />
                    </div>
                    <div className="col-sm-6">
                        <div className="statusSection">
                            <span className="statusTitle">Headset Name:</span>
                            <span className="statusResult">
                                <span className="headsetOnly">Epoc+</span>
                                <span className="noHeadsetOnly">No device</span>
                            </span>
                        </div>
                        <div className="statusSection">
                            <span className="statusTitle">
                                Update Frequency:
                            </span>
                            <span className="statusResult">
                                {this.state.updateHz}
                                Hz
                            </span>
                        </div>
                        <div className="statusSection">
                            <span className="statusTitle">
                                Connection Strength:
                            </span>
                            <span className="statusResult">
                                <Connection size={-1} />
                            </span>
                        </div>
                        <div className="statusSection">
                            <span className="statusTitle">Battery:</span>
                            <span className="statusResult">
                                <Battery size={-1} />{' '}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('bciUpdate', this.bciUpdate);
        }
    }
}

Status.propTypes = {};
Status.defaultProps = {};

export default Status;
