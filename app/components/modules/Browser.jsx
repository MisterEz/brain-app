import React from 'react';
import PropTypes from 'prop-types';

class Browser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    handleIconCallback = (action) => {
        this.props.moduleCallback(action);
    };
    render() {
        return (
            <div className="Browser">
                <ModuleIcon
                    title="3D Visualization"
                    moduleClass="3D"
                    imagePath="/assets/img/moduleIcons/3d@<width>.jpg"
                    imgStyle="dark"
                    text="Visualise brain activity in stunning 3D on an anatomically correct model of a brain."
                    iconCallback={this.handleIconCallback}
                />
                <ModuleIcon
                    title="Brain Wave Game"
                    moduleClass="BrainwaveGame"
                    imagePath="/assets/img/moduleIcons/game@<width>.jpg"
                    imgStyle="dark"
                    text="Try and navigate an asteroid field using nothing but the power of your mind!"
                    iconCallback={this.handleIconCallback}
                />
                <ModuleIcon
                    title="EEG Chart"
                    moduleClass="Eeg"
                    imagePath="/assets/img/moduleIcons/eeg@<width>.jpg"
                    text="See the raw data streaming in through your EEG headset and see how it behaves."
                    iconCallback={this.handleIconCallback}
                />
                <ModuleIcon
                    title="Bar Chart"
                    moduleClass="Bar2"
                    imagePath="/assets/img/moduleIcons/bar@<width>.jpg"
                    text="Look and compare the data in realtime across five frequency bands."
                    iconCallback={this.handleIconCallback}
                />
                <ModuleIcon
                    title="Status"
                    moduleClass="Status"
                    imagePath="/assets/img/moduleIcons/status@<width>.jpg"
                    text="Calibrate, check the status and fit your headset to get the best results."
                    iconCallback={this.handleIconCallback}
                />
                {/*
                <ModuleIcon
                    title="Dev Demo"
                    moduleClass="Demo"
                    iconCallback={this.handleIconCallback}
                />*/}
            </div>
        );
    }
}

Browser.propTypes = {
    moduleCallback: PropTypes.func
};

Browser.defaultProps = {};

class ModuleIcon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    handleClick = (e) => {
        e.preventDefault();
        this.props.iconCallback({
            open: this.props.moduleClass
        });

        if (window.Compatibility.isMobile) {
            window.scrollTo(0, 0);
        }
        this.hasRendered = true;
    };
    render() {
        let imgWidth = window.Compatibility.isMobile ? '600' : '350';
        let path = this.props.imagePath.replace('<width>', imgWidth);
        return (
            <div className={['ModuleIcon', this.props.imgStyle].join(' ')}>
                <div className="moduleIconInner">
                    <div className="moduleIconFrame">
                        <a
                            href="#"
                            className="expandy"
                            onClick={this.handleClick}
                        />
                        <div className="imgContainer">
                            <img
                                className="img-fluid"
                                src={path}
                                alt={this.props.title}
                            />
                        </div>
                        <div className="moduleIconTitle">
                            <h3>{this.props.title}</h3>
                            <p>{this.props.text}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
ModuleIcon.propTypes = {
    iconCallback: PropTypes.func,
    imagePath: PropTypes.string,
    title: PropTypes.string,
    moduleClass: PropTypes.string,
    imgStyle: PropTypes.string,
    text: PropTypes.string
};

ModuleIcon.defaultProps = {
    imagePath: 'assets/img/module_bg.jpg',
    title: 'No Title',
    imgStyle: 'light',
    text: 'No Text!'
};

export default Browser;
