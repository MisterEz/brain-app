import React from 'react';
import PropTypes from 'prop-types';
import ModuleOptions from '../ModuleOptions';
import Config from '../ThreeD/Config.jsx';
import { resizeComponents } from '../../Helpers.jsx';

class Bar2 extends React.Component {
    constructor(props) {
        super(props);
        this.containerRef = React.createRef();
        this.barRef = React.createRef();

        this.isRedrawn = false;
        this.scaleRanger = {};

        this.channels = window.bciConfig.channels;
        this.frequencies = window.bciConfig.frequencies;

        this.visibleSensors = {};
        this.visibleFrequencies = {};

        for (let i = 0; i < this.channels.length; i++) {
            this.visibleSensors['sensor' + i] = false;
        }

        for (let i = 0; i < this.frequencies.length; i++) {
            this.visibleFrequencies['frequency' + i] = false;
        }

        if (this.props.setOptionsCallback) {
            this.props.setOptionsCallback(
                function() {
                    this.setState((prevState) => {
                        return {
                            optionsOpen: !prevState.optionsOpen
                        };
                    });
                }.bind(this)
            );
        }

        this.resizeTimeout;
        if (typeof window !== 'undefined') {
            window.addEventListener(
                'resize',
                function() {
                    this.updateSize();

                    //There is a 250ms transition on module height which will fuck with everything
                    clearTimeout(this.resizeTimeout);
                    this.resizeTimeout = setTimeout(this.updateSize, 250);
                }.bind(this)
            );
        }

        this.state = {
            visibleSensors: this.visibleSensors,
            visibleFrequencies: this.visibleFrequencies,
            isResizing: false,
            scale: 20,
            optionsOpen: false,
            displayGrid: true,
            autoScale: false
        };
    }
    updateVisibleFrequencies = (e) => {
        let state = this.state.visibleFrequencies;
        state[e.target.name] = !state[e.target.name];

        let count = 0;
        let newFrequencies = [];

        for (let frequency in state) {
            if (state.hasOwnProperty(frequency)) {
                if (!state[frequency]) {
                    newFrequencies.push(this.frequencies[count]);
                }
                count++;
            }
        }

        this.barRef.current.updateFrequencies(newFrequencies);

        this.setState({ visibleFrequencies: state });
    };
    updateVisibleChannels = (e) => {
        let state = this.state.visibleSensors;
        state[e.target.name] = !state[e.target.name];

        let count = 0;
        let newChannels = [];

        for (let channels in state) {
            if (state.hasOwnProperty(channels)) {
                if (!state[channels]) {
                    newChannels.push(this.channels[count]);
                }
                count++;
            }
        }

        this.barRef.current.updateChannels(newChannels);

        this.setState({
            sensorSelectAll: false,
            visibleSensors: state
        });
    };
    updateGridState = () => {
        let state = !this.state.displayGrid;
        this.barRef.current.updateGridState(state);
        this.setState({ displayGrid: state });
    };
    setScale = (e) => {
        if (this.barRef.current.scale !== e.target.value) {
            this.barRef.current.updateScale(e.target.value);
        }

        this.setState({ scale: e.target.value });
    };
    updateScaleMode = () => {
        let state = !this.state.autoScale;
        this.barRef.current.updateScaleMode(state);

        if (!state) {
            this.barRef.current.updateScale(20);
            this.setState({
                scale: 20,
                autoScale: state
            });
        } else {
            this.scaleRanger = {};
            this.setState({
                autoScale: state
            });
        }
    };
    sensorSelectAll = () => {
        let state = !this.state.sensorSelectAll;
        let channelsState = this.state.visibleSensors;

        let count = 0;
        let newChannels = [];

        for (let channels in channelsState) {
            if (channelsState.hasOwnProperty(channels)) {
                channelsState[channels] = state;

                if (!channelsState[channels]) {
                    newChannels.push(this.channels[count]);
                }
                count++;
            }
        }

        this.barRef.current.updateChannels(newChannels);

        this.setState({
            sensorSelectAll: state,
            visibleSensors: channelsState
        });
    };
    sensorInvertSelection = () => {
        let state = this.state.visibleSensors;

        let count = 0;
        let newChannels = [];
        let allSelected = 0;

        for (let channels in state) {
            if (state.hasOwnProperty(channels)) {
                state[channels] = !state[channels];

                if (!state[channels]) {
                    allSelected++;
                    newChannels.push(this.channels[count]);
                }
                count++;
            }
        }

        if (allSelected === 0) {
            allSelected = true;
        } else {
            allSelected = false;
        }

        this.barRef.current.updateChannels(newChannels);
        this.setState({
            sensorSelectAll: allSelected,
            visibleSensors: state
        });
    };
    getOptions = () => {
        this.visibleSensors = [];
        this.visibleFrequencies = [];

        for (let i = 0; i < this.channels.length; i++) {
            this.visibleSensors.push({
                type: 'checkbox',
                text: 'Hide ' + this.channels[i],
                name: 'sensor' + i + '',
                value: this.state.visibleSensors['sensor' + i],
                onChange: this.updateVisibleChannels
            });
        }

        for (let i = 0; i < this.frequencies.length; i++) {
            this.visibleFrequencies.push({
                type: 'checkbox',
                text: 'Hide ' + this.frequencies[i],
                name: 'frequency' + i + '',
                value: this.state.visibleFrequencies['frequency' + i],
                onChange: this.updateVisibleFrequencies
            });
        }

        if (!this.state.autoScale) {
            this.scaleRanger = {
                type: 'ranger',
                text: 'Scale',
                desc: 'Maximum Value: ' + this.state.scale,
                name: 'scale',
                min: 1,
                max: 250,
                value: this.state.scale,
                onChange: this.setScale
            };
        }

        return [
            {
                type: 'submenu',
                text: 'Show/Hide Sensors',
                desc: '',
                options: [
                    {
                        type: 'button',
                        text: 'Invert Selection',
                        name: 'sensorInvertSelection',
                        onClick: this.sensorInvertSelection
                    },
                    {
                        type: 'checkbox',
                        text: 'Select All',
                        name: 'sensorSelectAll',
                        value: this.state.sensorSelectAll,
                        onChange: this.sensorSelectAll
                    },
                    {
                        type: 'list',
                        list: this.visibleSensors
                    },

                    {
                        type: 'hr'
                    }
                ]
            },
            {
                type: 'submenu',
                text: 'Show/Hide Waves',
                desc: '',
                options: [
                    {
                        type: 'list',
                        list: this.visibleFrequencies
                    }
                ]
            },
            {
                type: 'checkbox',
                text: 'Diplay Grid',
                name: 'displayGrid',
                value: this.state.displayGrid,
                onChange: this.updateGridState
            },
            {
                type: 'checkbox',
                text: 'Automatic Scale',
                name: 'autoScale',
                value: this.state.autoScale,
                onChange: this.updateScaleMode
            },
            this.scaleRanger
        ];
    };
    optionsCallback = (newState) => {
        this.setState(newState);
    };
    render() {
        return (
            <div style={{ height: '100%' }}>
                <div
                    ref={this.containerRef}
                    className="Bar2 moduleHeightStretch"
                >
                    <BarChart
                        ref={this.barRef}
                        frequencies={this.frequencies}
                        channels={this.channels}
                        isResizing={this.props.isResizing}
                    />
                </div>
                <ModuleOptions
                    open={this.state.optionsOpen}
                    options={this.getOptions()}
                    optionsCallback={this.optionsCallback}
                />
            </div>
        );
    }
    componentDidMount() {
        resizeComponents();
    }
    componentWillUnmount() {
        clearTimeout(this.resizeTimeout);
    }
    updateSize = () => {
        let width =
            this.containerRef.current.getBoundingClientRect().width - 45;
        let height =
            this.containerRef.current.getBoundingClientRect().height - 45;
        this.barRef.current.updateSize(width, height);
    };
    componentDidUpdate() {
        //inititate resizing
        if (this.props.isResizing && !this.state.isResizing) {
            this.setState({
                isResizing: true
            });
        }
        //End resizing
        if (!this.props.isResizing && this.state.isResizing) {
            this.isRedrawn = false;
            this.setState({
                isResizing: false
            });
        }

        if (
            !this.props.isResizing &&
            !this.state.isResizing &&
            !this.isRedrawn
        ) {
            this.updateSize();
        }
    }
}

class BarChart extends React.Component {
    constructor(props) {
        super(props);

        this.channels = this.props.channels;
        this.frequencies = this.props.frequencies;
        this.scale = 20;
        this.isRedrawn = true;
        this.displayGrid = true;
        this.colours = {};
        this.autoScale = false;
        this.averages = 0;

        let colours = [
            Config.thetaColorBAR,
            Config.alphaColorBAR,
            Config.lowBetaColorBAR,
            Config.highBetaColorBAR,
            Config.gammaColorBAR
        ];

        for (let i = 0; i < window.bciConfig.frequencies.length; i++) {
            this.colours['' + window.bciConfig.frequencies[i]] = colours[i];
        }

        //Support for retina displays

        //This would be pixel perfect but that might be too small
        //this.dpr = window.devicePixelRatio || 1;
        this.dpr =
            window.devicePixelRatio > 1
                ? Math.round(window.devicePixelRatio / 2)
                : 1;

        this.values = new Array(this.channels.length)
            .fill()
            .map(() => ({ values: [] }));

        for (let i = 0; i < this.channels.length; i++) {
            this.values[i] = new Array(this.frequencies.length)
                .fill()
                .map(() => ({ values: [] }));
        }

        this.state = {
            width: 1315,
            height: 525,
            isResizing: true
        };

        if (typeof window !== 'undefined') {
            window.addEventListener('bciUpdate', this.bciUpdate);
        }
    }
    bciUpdate = (e) => {
        let value;
        for (let i = 0; i < this.channels.length; i++) {
            for (let j = 0; j < this.frequencies.length; j++) {
                value =
                    e.detail.channels[this.channels[i]][this.frequencies[j]];

                if (
                    this.autoScale &&
                    (value < this.averages * 8 || this.averages === 0)
                ) {
                    this.averages = (this.averages + value) / 2;
                }

                if (
                    this.autoScale &&
                    value > this.scale &&
                    value < this.averages * 16
                ) {
                    this.scale = value;
                    this.loadBackground();
                }

                this.values[i][j] = value;
            }
        }
        this.drawGraph();
    };
    updateSize = (width, height) => {
        //This should match the css rules
        if (!window.Compatibility.isMobile) {
            if (width < 250) {
                width = 250;
            } else if (height < 300) {
                height = 300;
            }
        }

        this.isRedrawn = false;
        this.setState({ width: width, height: height });
    };
    updateScaleMode = (state) => {
        if (state) {
            this.scale = 0;
            this.averages = 0;
        }
        this.autoScale = state;
    };
    updateScale = (scale) => {
        this.scale = scale;
        this.loadBackground();
    };
    updateChannels = (channels) => {
        this.channels = channels;
        this.loadBackground();
    };
    updateFrequencies = (frequencies) => {
        this.frequencies = frequencies;
    };
    updateGridState = (state) => {
        this.displayGrid = state;
    };
    loadBackground = () => {
        let context = this.canvas.getContext('2d');
        let width = this.state.width;
        let height = this.state.height;
        let markerHeight,
            majorMarker,
            x1,
            x2,
            legendWidth,
            barWidth,
            x,
            y,
            sensorWidth,
            totalBars;

        width *= this.dpr;
        height *= this.dpr;

        context.beginPath();
        context.clearRect(-10, -10, width + 100, height + 100);

        context.font = 'bold 12pt sans-serif';
        context.textAlign = 'start';

        totalBars =
            this.channels.length * this.frequencies.length +
            this.channels.length;

        barWidth =
            width /
            (window.bciConfig.channels.length *
                window.bciConfig.frequencies.length);

        let titles = [[]];
        legendWidth = 0;

        if (width < 450) {
            barWidth = 0.05 * width;
        }

        let index = 0;
        for (let i = 0; i < this.frequencies.length; i++) {
            if (
                legendWidth +
                    barWidth +
                    context.measureText(this.frequencies[i]).width +
                    20 <
                width
            ) {
                legendWidth +=
                    barWidth +
                    context.measureText(this.frequencies[i]).width +
                    20;
                titles[index].push(this.frequencies[i]);
            } else {
                index++;
                titles.push([]);
                legendWidth =
                    barWidth +
                    context.measureText(this.frequencies[i]).width +
                    20;
                titles[index].push(this.frequencies[i]);
            }
        }

        legendWidth = [0, 0];

        //Draw Legend
        legendWidth[0] = titles[0].length * barWidth * 2;

        if (titles.length > 1) {
            legendWidth[1] = titles[1].length * barWidth * 2;
        }

        x1 = 0.1 * width;

        if (titles.length === 1) {
            y = 22;
        } else {
            y = 15;
        }

        for (let i = 0; i < titles.length; i++) {
            for (let j = 0; j < titles[i].length; j++) {
                context.fillStyle =
                    'rgb(' +
                    this.colours[this.frequencies[i * titles[0].length + j]] +
                    ')';
                context.fillRect(
                    x1 + context.measureText(titles[i][j]).width + 10,
                    0 + i * 30 + (15 - (barWidth * this.dpr) / 2),
                    barWidth * this.dpr,
                    barWidth * this.dpr
                );

                context.fillStyle = '#333';
                context.fillText(titles[i][j], x1, y + i * y * 2);

                x1 += barWidth + context.measureText(titles[i][j]).width + 20;
            }

            if (titles.length > 1) {
                if (titles[1].length > 1) {
                    x1 = 0.1 * width + legendWidth[1] / 2;
                } else {
                    x1 = width / 2 - 0.1 * width;
                }
            }
        }

        context.font = 'bold 10pt sans-serif';
        context.textAlign = 'center';

        //Draw axis
        context.lineWidth = 2;
        context.strokeStyle = '#333';
        context.fillStyle = '#333';

        context.moveTo(52.6, 0.15 * height);
        context.lineTo(52.6, 0.9 * height);

        //Y-axis line markers
        for (let i = 0; i < 25; i++) {
            markerHeight = (0.15 + i * 0.03) * height;
            context.moveTo(52.6, markerHeight);
            //Draw major marker else minor marker
            if (i % 5 === 0) {
                context.lineTo(39.45, markerHeight);
                majorMarker = ((25 - i) / 25) * this.scale;
                context.fillText(
                    Number(majorMarker.toFixed(2)),
                    19.725,
                    markerHeight
                );
            } else {
                context.lineTo(46.025, markerHeight);
            }
        }

        x1 = 52.6 + 0.005 * width;
        //Width - outside graph padding - Y-axis to first bar padding
        width = width - 78.9 - 0.005 * width;

        //actual bars + padding between sensors.
        totalBars =
            this.channels.length * this.frequencies.length +
            this.channels.length;

        // all frequencies for bars + 1 bar / 2 for padding on each side.
        sensorWidth =
            (width / totalBars) * this.frequencies.length + width / totalBars;

        x2 = x1 + sensorWidth;

        //X-axis line markers
        for (let i = 0; i < this.channels.length; i++) {
            context.moveTo(x1, 0.9 * height);
            context.lineTo(x1, 0.94 * height);

            context.save();

            if (this.state.width < 550) {
                context.translate(x1 + (x2 - x1) / 2, 0.97 * height);
                context.rotate(((1 - width / 550) * -75 * Math.PI) / 180);
                x = 0;
                y = 0;
            } else {
                x = x1 + (x2 - x1) / 2;
                y = 0.97 * height;
            }

            context.fillText(this.channels[i], x, y);

            context.restore();
            x1 += sensorWidth;
            x2 = x1 + sensorWidth;
        }

        context.moveTo(x1, 0.9 * height);
        context.lineTo(x1, 0.94 * height);

        //X-axis line
        context.moveTo(52.6, 0.9 * height);
        context.lineTo(x1 + 0.01 * this.state.width, 0.9 * height);
        context.stroke();
    };
    drawGraph = () => {
        let context = this.canvas.getContext('2d');
        let width = this.state.width;
        let height = this.state.height;
        let top,
            bottom,
            value,
            x,
            totalBars,
            barWidth,
            markerHeight,
            sensorWidth;

        width *= this.dpr;
        height *= this.dpr;

        context.beginPath();
        context.clearRect(
            51 + 0.0025 * width,
            0.1 * height,
            width,
            0.8 * height
        );

        //Draw gridlines
        if (this.displayGrid) {
            context.beginPath();
            context.lineWidth = 1;
            context.strokeStyle = '#e0dbd8';

            //Width - outside graph padding - Y-axis to first bar padding
            width = width - 78.9 - 0.005 * width;

            x = 52.6 + 0.005 * this.state.width;

            //actual bars + padding between sensors.
            totalBars =
                this.channels.length * this.frequencies.length +
                this.channels.length;

            // all frequencies for bars + 1 bar / 2 for padding on each side.
            sensorWidth =
                (width / totalBars) * this.frequencies.length +
                width / totalBars;

            //X-axis gridlines
            for (let i = 0; i < this.channels.length; i++) {
                context.moveTo(x, 0.9 * height);
                context.lineTo(x, 0.15 * height);
                x += sensorWidth;
            }
            context.moveTo(x, 0.9 * height);
            context.lineTo(x, 0.15 * height);

            //Y-axis gridlines
            for (let i = 0; i < 25; i += 5) {
                markerHeight = (0.15 + i * 0.03) * height;
                context.moveTo(54, markerHeight);
                context.lineTo(x, markerHeight);
            }
            context.stroke();
        }

        //Draw bars
        context.beginPath();
        x = 52.6 + 0.005 * width;

        totalBars =
            this.channels.length * this.frequencies.length +
            this.channels.length;

        barWidth = width / totalBars;

        x += barWidth / 2;

        for (let i = 0; i < this.channels.length; i++) {
            for (let j = 0; j < this.frequencies.length; j++) {
                context.fillStyle =
                    'rgb(' + this.colours[this.frequencies[j]] + ')';
                value = this.values[i][j];
                top =
                    0.15 * height +
                    ((this.scale - value) / this.scale) * 0.75 * height;

                if (top < 0.15 * height) {
                    top = 0.15 * height;

                    //Draw data exceeds scale height indicator
                    context.beginPath();
                    context.moveTo(x + 0.25 * barWidth, 0.145 * height);
                    context.lineTo(x + 0.75 * barWidth, 0.145 * height);
                    context.lineTo(x + barWidth / 2, 0.13 * height);
                    context.fill();
                }

                if (value / this.scale > 1) {
                    bottom = 0.75 * height;
                } else {
                    bottom = (value / this.scale) * 0.75 * height;
                }

                context.fillRect(x, top, barWidth - 1, bottom);
                x += barWidth;
            }
            x += barWidth;
        }
        context.beginPath();
        context.lineWidth = 3;
        context.strokeStyle = '#333';
        context.moveTo(52.6, 0.9 * height);
        context.lineTo(x + 0.005 * width, 0.9 * height);
        context.stroke();
    };
    render() {
        return (
            <canvas
                className="barChart2"
                ref={(ref) => (this.canvas = ref)}
                width={this.state.width * this.dpr}
                height={this.state.height * this.dpr}
            />
        );
    }
    componentDidMount() {
        this.loadBackground();
    }

    componentDidUpdate() {
        if (!this.isRedrawn) {
            this.isRedrawn = true;
            this.loadBackground();
        }
    }

    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('bciUpdate', this.bciUpdate);
        }
    }
}

Bar2.propTypes = {
    isResizing: PropTypes.bool,
    setOptionsCallback: PropTypes.func
};
Bar2.defaultProps = {
    isResizing: false
};

BarChart.propTypes = {
    channels: PropTypes.array,
    frequencies: PropTypes.array,
    scale: PropTypes.number
};
BarChart.defaultProps = {};

export default Bar2;
