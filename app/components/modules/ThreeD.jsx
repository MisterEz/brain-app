import React from 'react';
import PropTypes from 'prop-types';
import init3D from '../ThreeD/init3D';
import ColorHelper from '../ThreeD/ColorHelper';
import Effects from '../ThreeD/Effects';
import Config from '../ThreeD/Config';
import ModuleOptions from '../ModuleOptions';
import Modal from '../Modal';
import { resizeComponents } from '../../Helpers.jsx';

class ThreeD extends React.Component {
    constructor(props) {
        super(props);

        this.lastMaterial = 'pbr';
        this.lastOverlay = 'invisible';

        this.state = {
            config: {},
            showEffectTexture: false,
            showFPS: false,
            material: this.lastMaterial,
            overlay: this.lastOverlay,
            rotateCamera: true,
            isResizing: false,
            noWebgl: false,
            optionsOpen: false,
            start: false,
            forceAmp: false
        };

        //Generate a unique ID
        this.canvasId =
            'threeDCanvas-' +
            Math.random()
                .toString(36)
                .substring(7);
        this.effectTexture = React.createRef();

        this.lastBciUpdate = null;
        this.lastChannels = null;

        if (typeof window !== 'undefined') {
            window.addEventListener('bciUpdate', this.bciUpdate);
        }

        if (this.props.setFullScreenFilter) {
            this.props.setFullScreenFilter(
                function(isFullscreen) {
                    console.log('IO AM TH THEADS filter', isFullscreen);
                    this.state.config.scene.nmSetFullscreen(isFullscreen);
                    return false;
                }.bind(this)
            );
        }

        if (this.props.setOptionsCallback) {
            this.props.setOptionsCallback(
                function() {
                    this.setState((prevState) => {
                        return {
                            optionsOpen: !prevState.optionsOpen
                        };
                    });
                }.bind(this)
            );
        }
    }
    bciUpdate = (e) => {
        if (this.state.config.scene === undefined) {
            return;
        }
        if (this.lastChannels === null) {
            this.lastChannels = e.detail.channels;
            return;
        }
        // Calculate the colors of each channel here and save them for later
        let singleChannelColors = {};
        for (let key in e.detail.channels) {
            if (e.detail.channels.hasOwnProperty(key)) {
                singleChannelColors[key] = ColorHelper.freqToColor4(
                    e.detail.channels[key],
                    this.lastChannels[key],
                    this.state.forceAmp
                );
            }
        }
        this.lastChannels = e.detail.channels;

        this.state.config.scene.bciState = e.detail.channels;
        this.state.config.scene.channelColors.push(singleChannelColors);
        this.state.config.scene.channelColors.shift();

        //TODO selective dynamic texture drawing?
        if (true || this.state.material.startsWith('dynamic')) {
            // Special god damn effects
            let effectTex = this.state.config.scene.getMaterialByName(
                'effectMat'
            ).emissiveTexture;
            Effects.renderOverlayTexture(
                effectTex.getContext(),
                this.state.config.scene.channelColors,
                0
            );
            effectTex.update();

            //Double the frame rate from 10fps to 20fps
            if (
                this.state.config.scene.optimzationLevel === 0 &&
                this.lastBciUpdate
            ) {
                window.setTimeout(() => {
                    Effects.renderOverlayTexture(
                        effectTex.getContext(),
                        this.state.config.scene.channelColors,
                        0.5
                    );
                    effectTex.update();
                }, (Date.now() - this.lastBciUpdate) / 2);
            }
            if (this.state.showEffectTexture) {
                Effects.renderOverlayTexture(
                    this.effectTexture.current.getContext('2d', {
                        alpha: false
                    }),
                    this.state.config.scene.channelColors,
                    0
                );
            }
        }
        this.lastBciUpdate = Date.now();
    };
    closeModal = () => {
        this.setState({ showBrowserWarning: false });
    };
    optionsCallback = (newState) => {
        this.setState(newState);
    };
    getOptions = () => {
        return [
            /*{
                type: 'checkbox',
                text: 'Show Sensors',
                desc: 'Show the sensor locations on the brain',
                name: 'showSensors',
                value: this.state.showSensors
            },*/
            {
                type: 'buttonGroup',
                text: 'Material',
                name: 'material',
                value: this.state.material, //Remember must be the same as 'name'
                btns: [
                    {
                        type: 'button',
                        name: 'disabled',
                        text: 'Disabled'
                    },
                    {
                        type: 'button',
                        name: 'pbr',
                        text: 'Realistic'
                    },
                    {
                        type: 'button',
                        name: 'transparent',
                        text: 'Transparent'
                    }
                ]
            },
            {
                type: 'buttonGroup',
                text: 'Overlay',
                name: 'overlay',
                value: this.state.overlay,
                btns: [
                    {
                        type: 'button',
                        name: 'disabled',
                        text: 'Disabled'
                    },
                    {
                        type: 'button',
                        name: 'point',
                        text: 'Point'
                    },
                    {
                        type: 'button',
                        name: 'wireframe',
                        text: 'Wireframe'
                    },
                    {
                        type: 'button',
                        name: 'invisible',
                        text: 'Transparent'
                    }
                ]
            },
            {
                type: 'checkbox',
                text: 'Rotate Brain',
                desc: 'Constant rotation of brain',
                name: 'rotateCamera',
                value: this.state.rotateCamera
            },
            {
                type: 'submenu',
                text: 'Debug',
                desc: 'FPS, dynamicTextures',
                options: [
                    {
                        type: 'checkbox',
                        text: 'Show dynamic texture',
                        desc: 'The dynamic effect layer in real time',
                        name: 'showEffectTexture',
                        value: this.state.showEffectTexture
                    },
                    {
                        type: 'checkbox',
                        text: 'Show FPS',
                        desc: 'Show the frame rate in the top left corner',
                        name: 'showFPS',
                        value: this.state.showFPS
                    },
                    {
                        type: 'checkbox',
                        text: 'Amplify Data',
                        desc: 'Amplify intensity to maximum',
                        name: 'forceAmp',
                        value: this.state.forceAmp
                    }
                ]
            }
        ];
    };

    render() {
        let modal = null;
        if (this.state.noWebgl) {
            modal = (
                <Modal
                    onClose={this.closeModal}
                    header={<h2>Your browser does not support WebGL</h2>}
                    body={
                        <div className="alert alert-danger">
                            <strong>Uh oh!</strong> Your browser won't be able
                            to run this feature.
                            <br />
                            <p>
                                We recommend{' '}
                                <a href="https://www.google.com/chrome/">
                                    Google Chrome
                                </a>{' '}
                                for the best experience using Brainstorm.
                            </p>
                        </div>
                    }
                    footer={
                        <div className="text-center">
                            <button
                                type="button"
                                className="btn btn-danger btn-block"
                                onClick={this.props.handleBrowse}
                            >
                                Take me back
                            </button>
                            <br />
                        </div>
                    }
                    closeable={false}
                />
            );
        }
        return (
            <div style={{ height: '100%' }}>
                {modal}
                <div className="ThreeD moduleHeightStretch">
                    <canvas className="renderCanvas" id={this.canvasId} />
                    <span
                        className={[
                            'fpsLabel',
                            this.state.showFPS ? 'visible' : 'hidden'
                        ].join(' ')}
                        id={this.canvasId + '-fpsLabel'}
                    >
                        0 fps
                    </span>
                    <canvas
                        className={[
                            'effectTexture',
                            this.state.showEffectTexture ? 'visible' : 'hidden'
                        ].join(' ')}
                        width={Config.effectTexDimensions[0]}
                        height={Config.effectTexDimensions[1]}
                        ref={this.effectTexture}
                    />
                </div>
                <ModuleOptions
                    open={this.state.optionsOpen}
                    options={this.getOptions()}
                    optionsCallback={this.optionsCallback}
                />
            </div>
        );
    }
    componentDidUpdate() {
        //Start resizing
        if (this.props.isResizing && !this.state.isResizing) {
            this.setState({
                isResizing: true
            });
        }

        //End resizing
        if (!this.props.isResizing && this.state.isResizing) {
            this.state.config.engine.resize();
            this.setState({
                isResizing: false
            });
        }

        if (this.state.config.scene === undefined) {
            return;
        }
        if (this.state.material !== this.lastMaterial) {
            this.lastMaterial = this.state.material;
            this.state.config.scene.nmChangeBrain(this.state.material);
        }
        if (this.state.overlay !== this.lastOverlay) {
            this.lastOverlay = this.state.overlay;
            this.state.config.scene.nmChangeOverlay(this.state.overlay);
        }
        this.state.config.scene.rotateCamera = this.state.rotateCamera;
    }
    componentDidMount() {
        if (!window.Compatibility.webgl || window.Compatibility.isIE) {
            this.setState({
                noWebgl: true
            });
            return;
        }

        resizeComponents();
        if (!window.depsLoaded) {
            let interval = setInterval(
                function() {
                    console.log(window.depsLoaded);
                    if (window.depsLoaded) {
                        clearInterval(interval);
                        this.doInitStuff();
                    }
                }.bind(this),
                1000
            );
            return;
        }
        this.doInitStuff();
    }
    doInitStuff = () => {
        let cfg = init3D.loadThreeD(this.canvasId);
        this.setState({
            config: cfg
        });

        cfg.scene.registerAfterRender(
            function() {
                if (this.state.showFPS) {
                    let fpsLabel = document.getElementById(
                        this.canvasId + '-fpsLabel'
                    );
                    fpsLabel.innerHTML = cfg.engine.getFps().toFixed() + ' fps';
                }
            }.bind(this)
        );
    };
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('bciUpdate', this.bciUpdate);
        }

        if (this.state.config.scene !== undefined) {
            //tidyup
            this.state.config.scene.dispose();
            this.state.config.engine.dispose();
        }
    }
}

ThreeD.propTypes = {
    isResizing: PropTypes.bool,
    setFullScreenFilter: PropTypes.func,
    handleBrowse: PropTypes.func,
    setOptionsCallback: PropTypes.func
};

ThreeD.defaultProps = {
    isResizing: false
};

export default ThreeD;
