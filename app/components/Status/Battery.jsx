import React from 'react';
import PropTypes from 'prop-types';

// GUI icon for
// http://emotiv.github.io/community-sdk/_i_emo_state_d_l_l_8h.html#aaee78abaa1dd794fd581dd8101e8db0f
class Battery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            batteryLevel: 0
        };

        if (typeof window !== 'undefined') {
            window.addEventListener('batteryUpdate', this.statusUpdate);
        }
    }
    // This function will get called when there is new information from the BCI
    statusUpdate = (e) => {
        this.setState({
            batteryLevel: e.detail.batteryLevel
        });
    };
    render() {
        if (this.props.size === -1) {
            return <span>{this.state.batteryLevel}</span>;
        }

        let sizeClass = ['batSmall', 'batMedium', 'batLarge'][this.props.size];
        let batteryIcon = 'flaticon-battery-1'; ///-1 -2 -3

        if (this.state.batteryLevel >= 4) {
            batteryIcon = 'flaticon-battery-3';
        } else if (this.state.batteryLevel >= 2) {
            batteryIcon = 'flaticon-battery';
        } else if (this.state.batteryLevel >= 1) {
            batteryIcon = 'flaticon-battery-2';
        } else {
            batteryIcon = 'flaticon-battery-1';
        }

        return (
            <span className={['Battery', sizeClass].join(' ')}>
                <span className="batteryText">
                    <i
                        className={['fi', batteryIcon].join(' ')}
                        aria-hidden="true"
                        title={this.state.batteryPercent + '%'}
                    />
                </span>
            </span>
        );
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('batteryUpdate', this.statusUpdate);
        }
    }
}

Battery.propTypes = {
    size: PropTypes.number
};

Battery.defaultProps = {
    size: 1
};

export default Battery;
