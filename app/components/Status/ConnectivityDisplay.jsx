import React from 'react';
import PropTypes from 'prop-types';

// GUI icon for
// http://emotiv.github.io/community-sdk/_i_emo_state_d_l_l_8h.html#aaee78abaa1dd794fd581dd8101e8db0f
class ConnectivityDisplay extends React.Component {
    constructor(props) {
        super(props); //Read props

        this.state = {};
    }
    render() {
        let els = [];
        for (let key in this.props.sensors) {
            if (this.props.sensors.hasOwnProperty(key)) {
                if (key === 'FP1' || key === 'FP2') {
                    continue;
                }

                let strength = this.props.sensors[key];
                strength = strength.split(' ').join('_');

                els.push(
                    <div
                        className={['cdSensorContainer', 'cd-' + key].join(' ')}
                        key={key}
                    >
                        <div className={['cdSensor', strength].join(' ')}>
                            <span className="cdLabel">{key}</span>
                        </div>
                    </div>
                );
            }
        }

        return (
            <div className="ConnectivityDisplay">
                <div className="cdHead center-block">{els}</div>
            </div>
        );
    }
}

ConnectivityDisplay.propTypes = {
    sensors: PropTypes.object
};

ConnectivityDisplay.defaultProps = {};

export default ConnectivityDisplay;
