import React from 'react';
import PropTypes from 'prop-types';
import ConnectivityDisplay from './ConnectivityDisplay';

// GUI icon for
// http://emotiv.github.io/community-sdk/_i_emo_state_d_l_l_8h.html#a16738742f662bb7ff33d7a7a14b60683
class SensorQuality extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            channelQuality: {
                F7: 'No Signal',
                T7: 'No Signal',
                T8: 'No Signal',
                AF3: 'No Signal',
                F3: 'No Signal',
                FC5: 'No Signal',
                CMS: 'No Signal',
                P7: 'No Signal',
                O1: 'No Signal',
                F4: 'No Signal',
                AF4: 'No Signal',
                F8: 'No Signal',
                FC6: 'No Signal',
                DRL: 'No Signal',
                P8: 'No Signal',
                O2: 'No Signal'
            }
        };

        if (typeof window !== 'undefined') {
            window.addEventListener('sensorsUpdate', this.statusUpdate);
        }
    }
    // This function will get called when there is new information from the BCI
    statusUpdate = (e) => {
        this.setState({
            channelQuality: e.detail.sensors
        });
    };
    render() {
        let channelQuality = this.state.channelQuality;
        if (this.props.size === 1) {
            return <ConnectivityDisplay sensors={channelQuality} />;
        }

        let points = 0;
        for (let channel in channelQuality) {
            if (channelQuality.hasOwnProperty(channel)) {
                switch (channelQuality[channel]) {
                    case 'No Signal':
                        points += 0;
                        break;

                    case 'Very Bad':
                        points += 1;
                        break;

                    case 'Poor':
                        points += 2;
                        break;

                    case 'Fair':
                        points += 3;
                        break;

                    case 'Good':
                        points += 5;
                        break;
                }
            }
        }
        let totalQuality = points / 90; //18*5

        if (this.props.size === -1) {
            return <span>{Math.round(totalQuality * 100)}%</span>;
        }

        return (
            <span className="SensorQuality">
                <span>{Math.round(totalQuality * 100)}%</span>
            </span>
        );
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('sensorsUpdate', this.statusUpdate);
        }
    }
}

SensorQuality.propTypes = {
    size: PropTypes.number
};

SensorQuality.defaultProps = {
    size: 1
};

export default SensorQuality;
