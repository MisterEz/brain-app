import React from 'react';
import PropTypes from 'prop-types';

// GUI icon for
// http://emotiv.github.io/community-sdk/_i_emo_state_d_l_l_8h.html#abac6dfe9fbbe9f959432c59dd39b056b
class Connection extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            signalStrength: 'No Signal'
        };

        this.statusUpdate = this.statusUpdate.bind(this);
        if (typeof window !== 'undefined') {
            window.addEventListener('connectionUpdate', this.statusUpdate);
        }
    }
    // This function will get called when there is new information from the BCI
    statusUpdate(e) {
        this.setState({
            signalStrength: e.detail.signalStrength
        });
    }
    render() {
        if (this.props.size === -1) {
            return <span>{this.state.signalStrength}</span>;
        }

        let sizeClass = ['conSmall', 'conMedium', 'conLarge'][this.props.size];
        let connectionIcon = 'flaticon-signal';

        if (this.state.signalStrength === 'Good Signal') {
            connectionIcon = 'flaticon-cell-phone-high-signal-indicator';
        } else if (this.state.batteryPercent === 'Bad Signal') {
            connectionIcon = 'flaticon-high-signal-indicator';
        } else {
            connectionIcon = 'flaticon-signal';
        }

        return (
            <span className={['Connection', sizeClass].join(' ')}>
                <span className="connectionText">
                    <i
                        className={['fi', connectionIcon].join(' ')}
                        aria-hidden="true"
                        title={this.state.signalStrength}
                    />
                </span>
            </span>
        );
    }
    componentWillUnmount() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('connectionUpdate', this.statusUpdate);
        }
    }
}

Connection.propTypes = {
    size: PropTypes.number
};

Connection.defaultProps = {
    size: 1
};

export default Connection;
