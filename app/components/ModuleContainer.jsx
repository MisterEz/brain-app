import React from 'react';
import PropTypes from 'prop-types';
import Browser from './modules/Browser';
import Eeg from './modules/Eeg';
import Bar2 from './modules/Bar2';
import ThreeD from './modules/ThreeD';
import Status from './modules/Status';
import Demo from './modules/Demo';
import BrainwaveGame from './modules/BrainwaveGame';

/* Container for modules, basically just a shell
 */
class ModuleContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleClass: this.props.initModule,
            lastWidth: this.props.width,
            isFullScreen: false,
            isFullScreenResizing: false,
            isResizing: false,
            showCloseHelper: false,
            optionsCallback: null
        };

        this.timeoutID;
        this.helperTimerId;

        this.fullscreenFilter;
        this.optionsCallback;

        this.ref = React.createRef();
    }
    handleModuleCallback = (action) => {
        if ('open' in action) {
            this.setState({
                moduleClass: action.open
            });
        }
    };
    setFullScreenFilter = (fuct) => {
        this.fullscreenFilter = fuct;
    };
    setOptionsCallback = (fuct) => {
        this.setState({ optionsCallback: fuct });
    };
    handleFullscreen = (e) => {
        e.preventDefault();
        let fullscreen = false;
        if (e.type === 'click') {
            fullscreen = !this.state.isFullScreen;
        } else if (e.type === 'keydown') {
            if (e.keyCode === 27) {
                fullscreen = false;
            }
        }

        if (
            typeof this.fullscreenFilter === 'function' &&
            !this.fullscreenFilter(fullscreen)
        ) {
            return;
        }
        clearTimeout(this.timeoutID);
        this.timeoutID = setTimeout(
            function() {
                this.setState({ isFullScreenResizing: false });
            }.bind(this),
            300
        );
        this.setState({
            isFullScreen: fullscreen,
            isFullScreenResizing: true,
            showCloseHelper: false
        });

        if (fullscreen) {
            window.addEventListener('keydown', this.handleFullscreen, false);
        } else {
            window.removeEventListener('keydown', this.handleFullscreen, false);
        }
    };
    handleOptions = () => {
        this.state.optionsCallback();
    };
    handleBrowse = (e) => {
        e.preventDefault();
        this.fullscreenFilter = null;

        this.setState((prevState, prevProps) => ({
            moduleClass: 'Browser',
            showCloseHelper: !prevProps.solo,
            optionsCallback: null
        }));

        clearTimeout(this.helperTimerId);
        this.helperTimerId = setTimeout(
            function() {
                this.setState({
                    showCloseHelper: false
                });
            }.bind(this),
            5000
        );
    };
    handleHoriSplit = (e) => {
        e.preventDefault();
        this.props.containerCallback({
            hsplit: this.props.id
        });

        if (this.state.isFullScreen) {
            this.handleFullscreen(e);
        }
    };
    handleVertSplit = (e) => {
        e.preventDefault();
        this.props.containerCallback({
            vsplit: this.props.id
        });
        if (this.state.isFullScreen) {
            this.handleFullscreen(e);
        }
    };
    handleClose = (e) => {
        e.preventDefault();

        //Request Close
        this.props.containerCallback({
            close: this.props.id
        });

        //Set to browser (if close is rejected this will take effect)
        this.handleBrowse(e);
    };
    calcWidthClass = () => {
        let className = '';
        let width = this.props.width;
        if (width >= 80) {
            className = 'lg';
        } else if (width >= 50) {
            className = 'md';
        } else if (width >= 30) {
            className = 'sm';
        } else if (width > 4) {
            className = 'xs';
        } else {
            className = 'xs min';
        }
        return className;
    };
    render() {
        let module = null;
        let moduleName = null;
        let isResizing =
            this.state.isResizing || this.state.isFullScreenResizing;

        switch (this.state.moduleClass) {
            case 'Browser':
                module = (
                    <Browser
                        moduleCallback={this.handleModuleCallback}
                        setOptionsCallback={this.setOptionsCallback}
                    />
                );
                moduleName = 'Select a module';
                break;
            case 'Eeg':
                module = (
                    <Eeg
                        isResizing={isResizing}
                        setOptionsCallback={this.setOptionsCallback}
                    />
                );
                moduleName = 'EEG Graph';
                break;
            case 'Bar2':
                module = (
                    <Bar2
                        isResizing={isResizing}
                        setOptionsCallback={this.setOptionsCallback}
                    />
                );
                moduleName = 'Bar Chart';
                break;
            case '3D':
                module = (
                    <ThreeD
                        isResizing={isResizing}
                        setFullScreenFilter={this.setFullScreenFilter}
                        handleBrowse={this.handleBrowse}
                        setOptionsCallback={this.setOptionsCallback}
                    />
                );
                moduleName = '3D Visualization';
                break;
            case 'Status':
                module = (
                    <Status setOptionsCallback={this.setOptionsCallback} />
                );
                moduleName = 'Status';
                break;
            case 'Demo':
                module = <Demo setOptionsCallback={this.setOptionsCallback} />;
                moduleName = '_DEVELOPER_ Demo';
                break;
            case 'BrainwaveGame':
                module = (
                    <BrainwaveGame
                        isResizing={isResizing}
                        setOptionsCallback={this.setOptionsCallback}
                    />
                );
                moduleName = 'Brainwave Game';
                break;
            default:
                module = (
                    <div className="alert alert-danger">
                        ERROR: module "{this.state.moduleClass}" not recognised{' '}
                    </div>
                );
                moduleName = 'Error loading module';
                break;
        }
        return (
            <div
                ref={this.ref}
                className={[
                    'ModuleContainer',
                    this.calcWidthClass(),
                    this.state.isFullScreen ? 'fullscreen' : 'notFullscreen'
                ].join(' ')}
            >
                <div
                    className="moduleInner moduleHeightStretch white_bg"
                    data-height={this.props.height}
                >
                    <div className="moduleTopbar clearfix">
                        <h4 className="moduleTitle">{moduleName}</h4>
                        <div className="moduleButtons" role="group">
                            {!window.Compatibility.isMobile ? (
                                <button
                                    type="button"
                                    className="btn btn-default"
                                    title="Split Horizontal"
                                    onClick={this.handleHoriSplit}
                                    disabled={!this.props.hextendable}
                                >
                                    <span
                                        className="fi flaticon-split-browser-1"
                                        aria-hidden="true"
                                    />
                                </button>
                            ) : null}

                            {!window.Compatibility.isMobile ? (
                                <button
                                    type="button"
                                    className="btn btn-default"
                                    title="Split Vertical"
                                    onClick={this.handleVertSplit}
                                    disabled={!this.props.vextendable}
                                >
                                    <span
                                        className="fi flaticon-split-browser"
                                        aria-hidden="true"
                                    />
                                </button>
                            ) : null}

                            {!window.Compatibility.isMobile ? (
                                <span className="buttonHspace" />
                            ) : null}

                            {//On mobile only show fullscreen button if there is a dedicated filter
                            !window.Compatibility.isMobile ||
                            typeof this.fullscreenFilter === 'function' ? (
                                <button
                                    type="button"
                                    className="btn btn-default pulse"
                                    title="Fullscreen"
                                    onClick={this.handleFullscreen}
                                >
                                    <span
                                        className={[
                                            'fi',
                                            'flaticon-fullscreen',
                                            this.state.isFullScreen
                                                ? 'text-primary'
                                                : ''
                                        ].join(' ')}
                                        aria-hidden="true"
                                    />
                                </button>
                            ) : null}

                            <button
                                type="button"
                                className="btn btn-default spin"
                                title="Options"
                                onClick={this.handleOptions}
                                disabled={
                                    !(
                                        typeof this.state.optionsCallback ===
                                        'function'
                                    )
                                }
                            >
                                <span
                                    className={[
                                        'fi',
                                        'flaticon-gear-option'
                                    ].join(' ')}
                                    aria-hidden="true"
                                />
                            </button>

                            {this.state.moduleClass !== 'Browser' ? (
                                <button
                                    type="button"
                                    className="btn btn-default"
                                    title="Return to browse"
                                    onClick={this.handleBrowse}
                                    disabled={
                                        this.state.moduleClass === 'Browser'
                                    }
                                >
                                    <span
                                        className="fi flaticon-home-page"
                                        aria-hidden="true"
                                    />
                                </button>
                            ) : null}

                            <button
                                type="button"
                                className={[
                                    'btn',
                                    'btn-default',
                                    'closeButton',
                                    this.state.moduleClass === 'Browser'
                                        ? 'visible'
                                        : 'hidden'
                                ].join(' ')}
                                title="Close"
                                onClick={this.handleClose}
                                disabled={this.props.solo}
                            >
                                <span
                                    className="fi flaticon-forbidden-mark"
                                    aria-hidden="true"
                                />
                            </button>
                        </div>
                    </div>

                    <div className="moduleBody">{module}</div>

                    {this.state.isFullScreen ? (
                        <div className="toolbarHelper fullscreenHelper">
                            Click again exit fullscreen
                        </div>
                    ) : null}
                    {this.state.showCloseHelper ? (
                        <div className="toolbarHelper closeHelper">
                            Click again to close
                        </div>
                    ) : null}
                </div>
            </div>
        );
    }
    static getDerivedStateFromProps(props, state) {
        if (props.width !== state.lastWidth) {
            return {
                lastWidth: props.width,
                isResizing: true
            };
        }
        if (props.isResizing !== state.isResizing) {
            return {
                isResizing: props.isResizing
            };
        }
        return {};
    }

    componentWillUnmount() {
        clearTimeout(this.helperTimerId);
        clearTimeout(this.timeoutID);
    }
}

ModuleContainer.propTypes = {
    containerCallback: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number,
    isResizing: PropTypes.bool,
    id: PropTypes.string,
    initModule: PropTypes.string,
    solo: PropTypes.bool,
    hextendable: PropTypes.bool,
    vextendable: PropTypes.bool
};

ModuleContainer.defaultProps = {
    initModule: 'Browser'
};

export default ModuleContainer;
