import React from 'react';
import PropTypes from 'prop-types';

class HelpWindow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleExit = (e) => {
        e.preventDefault();
        this.props.menuCallback({
            openWindow: null
        });
    };

    render() {
        if (this.props.show === null) {
            return null;
        }

        let header = (
            <div className="shortHeader" onClick={this.handleExit}>
                <div className="container">
                    <h2>
                        <span
                            className="fi flaticon-arrowhead-thin-outline-to-the-left"
                            aria-hidden="true"
                        />
                        {this.props.show}
                    </h2>
                </div>
            </div>
        );

        let content = null;
        if (this.props.show === 'about') {
            content = (
                <div className="helpWindowBody">
                    <div className="container">
                        <div className="pull-right white_bg repoButton">
                            <a
                                href="https://gitlab.com/MisterEz/brain-app"
                                target="_blank"
                            >
                                <img
                                    src="assets/img/wm_no_bg.png"
                                    alt="gitlab"
                                />
                            </a>
                        </div>
                        <p>
                            <b>Designed &amp; built with love</b> by Nathan
                            Mitchell, Sean Nicoll, Michael Lyndon &amp; Kyle
                            Jacobson
                        </p>
                        <p>
                            Code licensed{' '}
                            <a
                                href="https://gitlab.com/MisterEz/brain-app/blob/master/LICENSE"
                                target="_blank"
                            >
                                MIT
                            </a>
                        </p>
                        <br />
                        <h4>Acknowledgements</h4>
                        <p>
                            Various site icons designed by Freepik, Linh Pham,
                            Google, Pavel Kozlov, Vaadin, Vectors Market &amp;
                            Smashicons from Flaticon
                        </p>
                    </div>
                </div>
            );
        } else if (this.props.show === 'help') {
            content = <iframe src="/helpDocs.html" />;
        }
        return (
            <div className="HelpWindow">
                {header}
                {content}
            </div>
        );
    }
}

HelpWindow.propTypes = {
    menuCallback: PropTypes.func,
    show: PropTypes.string
};

HelpWindow.defaultProps = {};

export default HelpWindow;
