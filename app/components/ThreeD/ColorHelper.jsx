import Config from './Config.jsx';

/* ColorHelper
 * Unprofessional color mixing and colors tools where color accuracy isn't important
 * should be faster than strict color correct methods but looks decent enough.
 */

class ColorHelper {
    static colorBy(color, strength, lastStr) {
        let newColor = color.slice(0);
        for (let i = 0; i < color.length; i++) {
            if (window.bciConfig.simulated) {
                if (strength > 87) {
                    newColor[i] *= strength * 2;
                }
            } else if (lastStr / strength > 1) {
                //if (strength > 95){
                newColor[i] *= (lastStr / strength) * 100;
            }
        }
        return newColor;
    }
    static colorCombine(colors) {
        let finalColor = [0, 0, 0];
        for (let i = 0; i < colors.length; i++) {
            let color = colors[i];
            for (let j = 0; j < color.length; j++) {
                finalColor[j] += color[j];
            }
        }
        return [
            finalColor[0] / 60000,
            finalColor[1] / 60000,
            finalColor[2] / 60000
        ];
    }
    static freqToColor4(freqs, lastFreqs, forceAmp = false) {
        let thetaColor = Config.thetaColor;
        let alphaColor = Config.alphaColor;
        let lowBetaColor = Config.lowBetaColor;
        let highBetaColor = Config.highBetaColor;
        let gammaColor = Config.gammaColor;

        let thetaOut = this.colorBy(thetaColor, freqs.theta, lastFreqs.theta);
        let alphOut = this.colorBy(alphaColor, freqs.alpha, lastFreqs.alpha);
        let lowBetaOut = this.colorBy(
            lowBetaColor,
            freqs.lowbeta,
            lastFreqs.lowbeta
        );
        let highBetaOut = this.colorBy(
            highBetaColor,
            freqs.highbeta,
            lastFreqs.highbeta
        );
        let gammaOut = this.colorBy(gammaColor, freqs.gamma, lastFreqs.gamma);

        let pow = 0;
        if (forceAmp) {
            pow = 1;
        } else {
            pow =
                Math.max(
                    freqs.theta,
                    freqs.alpha,
                    freqs.lowbeta,
                    freqs.highbeta,
                    freqs.gamma
                ) / 100;

            pow = Math.min(0.6 + pow, 1);
        }

        let finalColor = this.colorCombine([
            thetaOut,
            alphOut,
            lowBetaOut,
            highBetaOut,
            gammaOut
        ]);
        return [finalColor[0], finalColor[1], finalColor[2], pow];
    }
}
export default ColorHelper;
