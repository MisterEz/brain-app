/* Config
 * Useful configuration stuff
 */
class Config {
    static setColors(colorArray) {
        this.thetaColor = colorArray[0];
        this.alphaColor = colorArray[1];
        this.lowBetaColor = colorArray[2];
        this.highBetaColor = colorArray[3];
        this.gammaColor = colorArray[4];
    }
    static setColorScheme(name) {
        if (name === 'test1') {
            this.thetaColor = [13, 38, 255];
            this.alphaColor = [12, 232, 231];
            this.lowBetaColor = [0, 255, 5];
            this.highBetaColor = [232, 221, 12];
            this.gammaColor = [255, 154, 0];
        }
    }
}

// Color pallet for effect texture generation
Config.thetaColor = [255, 60, 37];
Config.alphaColor = [255, 178, 53];
Config.lowBetaColor = [0, 176, 251];
Config.highBetaColor = [165, 255, 64];
Config.gammaColor = [255, 0, 174];

Config.alphaColorEEG = [224, 141, 4];
Config.highBetaColorEEG = [90, 173, 0];

Config.thetaColorBAR = [255, 92, 74];
Config.alphaColorBAR = [255, 178, 53];
Config.lowBetaColorBAR = [45, 192, 255];
Config.highBetaColorBAR = [165, 255, 64];
Config.gammaColorBAR = [255, 55, 190];

//How many eeg samples to buffer and render on the effect textures
Config.queueLength = 15;

//Positions of sensors on the brain_pial UV map as percentages
// "accurate"
Config.realMap = {
    O2: [0.22, 0.93],
    P8: [0.165, 0.862],
    T8: [0.199, 0.803],
    FC6: [0.33, 0.697],
    F4: [0.395, 0.655],
    F8: [0.29, 0.6],
    AF4: [0.36, 0.553],

    O1: [0.825, 0.479],
    P7: [0.79, 0.422],
    T7: [0.76, 0.37],
    FC5: [0.798, 0.217],
    F3: [0.841, 0.156],
    F7: [0.692, 0.193],
    AF3: [0.74, 0.077]
};

// spaced out for better effect
Config.sexyMap = {
    O2: [0.257, 0.8975],
    P8: [0.1685, 0.8865],
    T8: [0.199, 0.8115],
    FC6: [0.3255, 0.7195],
    F4: [0.4065, 0.6675],
    F8: [0.2945, 0.61],
    AF4: [0.3755, 0.5685],

    O1: [0.847, 0.416],
    P7: [0.799, 0.4295],
    T7: [0.77, 0.385],
    FC5: [0.812, 0.2015],
    F3: [0.844, 0.143],
    F7: [0.724, 0.192],
    AF3: [0.7625, 0.094]
};

// EffectTexture dimensions
// Lowering the res is actually a really good cheap way to add gradients
Config.effectTexDimensions = [512, 512];

export default Config;
