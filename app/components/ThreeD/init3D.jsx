import Config from './Config';

/* init3D
 * Loads the scene for the 3d module
 */
class init3D {
    static loadThreeD(id) {
        BABYLON.IDBStorageEnabled = true;

        let canvas = document.getElementById(id);
        let engine = new BABYLON.Engine(canvas, false, null, false);

        engine.enableOfflineSupport = false;

        let scene = this.createScene(engine, canvas);

        engine.runRenderLoop(function() {
            scene.render();
        });

        window.addEventListener('resize', function() {
            engine.resize();
        });

        return {
            engine: engine,
            scene: scene,
            canvas: canvas
        };
    }

    static createScene(engine, canvas) {
        let scene = new BABYLON.Scene(engine);

        scene.autoClear = false;
        scene.autoClearDepthAndStencil = false;

        let options = BABYLON.SceneOptimizerOptions.ModerateDegradationAllowed();

        //Optimizer
        let optimizer = new BABYLON.SceneOptimizer(scene, options);
        optimizer.start();

        scene.optimzationLevel = 0;
        optimizer.onNewOptimizationAppliedObservable.add(function(optim) {
            scene.optimzationLevel = optim;
            console.log('Priorty Change: ', optim);
        });

        /////////////////////////
        // LIGHTS
        /////////////////////////

        let hemi = new BABYLON.HemisphericLight('hemi', BABYLON.Vector3.Up());
        hemi.intensity = 0.8;
        hemi.diffuse = new BABYLON.Color3(1, 0.7, 0.7);
        hemi.specular = new BABYLON.Color3(0.3, 0.2, 0.2);
        hemi.groundColor = new BABYLON.Color3(0.3, 0.3, 0.35);

        //Adding a light
        let light = new BABYLON.PointLight(
            'Omni',
            new BABYLON.Vector3(20, 20, 100),
            scene
        );
        light.intensity = 0.5;
        light.specular = new BABYLON.Color3(1, 1, 1);

        /////////////////////////
        // CAMERA
        /////////////////////////

        let camera = new BABYLON.ArcRotateCamera(
            'Camera',
            0,
            0.8,
            100,
            BABYLON.Vector3.Zero(),
            scene
        );
        camera.attachControl(canvas, false);
        camera.fov = 0.5;
        camera.radius = 200;
        camera.beta = 1.3;

        scene.activeCameras.push(camera);

        /////////////////////////
        // MATERIALS
        /////////////////////////

        // The transparent material used for showing the internal structure
        // xray
        function makeXrayMat() {
            let xrayMat = new BABYLON.StandardMaterial('xray', scene);
            xrayMat.emissiveColor = new BABYLON.Color3(0.4, 0.4, 0.4);
            xrayMat.alpha = 0.1;
            let fresnelParams = new BABYLON.FresnelParameters();
            fresnelParams.isEnabled = true;
            fresnelParams.leftColor = new BABYLON.Color3(0.5, 0.6, 1);
            fresnelParams.rightColor = new BABYLON.Color3(0, 0, 0);
            fresnelParams.power = 1;
            fresnelParams.bias = 0.1;
            let fresnelParams2 = new BABYLON.FresnelParameters();
            fresnelParams2.isEnabled = true;
            fresnelParams2.leftColor = new BABYLON.Color3(1, 1, 1);
            fresnelParams2.rightColor = new BABYLON.Color3(0.2, 0.2, 0.2);
            fresnelParams2.power = 1;
            fresnelParams2.bias = 0.5;
            xrayMat.emissiveFresnelParameters = fresnelParams;
            xrayMat.opacityFresnelParameters = fresnelParams2;
            xrayMat.backFaceCulling = false;
            xrayMat.disableLighting = true;
            xrayMat.alphaMode = BABYLON.Engine.ALPHA_ADD;

            return xrayMat;
        }

        let diffTex = new BABYLON.Texture('/assets/3d/diffuse3.jpg', scene);
        let specTex = new BABYLON.Texture('/assets/3d/spec.jpg', scene);
        let occTex = new BABYLON.Texture('/assets/3d/ao.jpg', scene);
        let normalTex = new BABYLON.Texture(
            '/assets/3d/hard_combine.jpg',
            scene
        );
        let shadowTex = new BABYLON.Texture('/assets/3d/shadowmap2.jpg', scene);
        let dynTex = new BABYLON.DynamicTexture(
            'effectMat',
            Config.effectTexDimensions[0],
            scene
        );

        // The normal realistic exterior material
        // defaultMat
        function makeDefaultMat() {
            let defaultMat = new BABYLON.StandardMaterial('defaultMat', scene);
            defaultMat.diffuseTexture = diffTex;
            defaultMat.specularTexture = specTex;
            defaultMat.bumpTexture = normalTex;
            defaultMat.lightmapTexture = shadowTex;
            defaultMat.useLightmapAsShadowmap = true;
            //defaultMat.ambientTexture = makeDynamicTexture().mat;

            defaultMat.specularPower = 200;
            defaultMat.backFaceCulling = true;
            return defaultMat;
        }

        // The lighting effects material
        // effectMat
        function makeEffectMat() {
            let effectMat = new BABYLON.StandardMaterial('effectMat', scene);

            effectMat.emissiveTexture = dynTex;
            effectMat.emissiveColor = new BABYLON.Color3(1, 1, 1);

            effectMat.disableLighting = true;
            effectMat.backFaceCulling = false;

            return effectMat;
        }
        function makePointEffectMat() {
            let effectMat = new BABYLON.StandardMaterial(
                'pointEffectMat',
                scene
            );

            effectMat.ambientTexture = dynTex;
            effectMat.opacityTexture = dynTex;
            //effectMat.alphaCutOff = 200;
            effectMat.alphaMode = BABYLON.Engine.ALPHA_ADD;

            effectMat.useAlphaFromDiffuseTexture = true;

            effectMat.emissiveTexture = dynTex;
            effectMat.emissiveColor = new BABYLON.Color3(1, 1, 1);

            effectMat.fillMode = effectMat.TriangleStripDrawMode;
            effectMat.pointSize = 3;

            effectMat.disableLighting = true;
            effectMat.backFaceCulling = false;

            return effectMat;
        }
        function makePBREffectMat() {
            // eslint-disable-line no-unused-vars
            let pbrMat = new BABYLON.PBRMaterial('pbrMat', scene);

            //let dynTex = new BABYLON.DynamicTexture('pbrMat', Config.effectTexDimensions[0], scene);
            pbrMat.albedoTexture = diffTex;
            pbrMat.ambientTextureStrength = 0.8;
            pbrMat.ambientTexture = occTex;
            pbrMat.reflectivityTexture = specTex;
            pbrMat.metallic = 0.2;
            pbrMat.roughness = 0.15;
            pbrMat.bumpTexture = normalTex;
            pbrMat.directIntensity = 0.5;

            //pbrMat.lightmapTexture = shadowTex;
            //pbrMat.useLightmapAsShadowmap = true;

            // TODO
            //pbrMat.fillMode  = 2;
            //pbrMat.pointSize = 5;

            //pbrMat.emissiveTexture = dynTex;
            //pbrMat.emissiveColor = new BABYLON.Color3(1,1,1);
            pbrMat.backFaceCulling = true;
            return pbrMat;
        }

        // A test material with a dynamic texture
        // dynamicTexture
        /*function makeDynamicTexture() {
            // Dynamic Material
            let myDynamicTexture = new BABYLON.DynamicTexture(
                'checkers',
                2000,
                scene
            );
            drawCheckeredBackground(myDynamicTexture.getContext(), 0);
            myDynamicTexture.update();

            function drawCheckeredBackground(ctx, offset) {
                let nRow = 80;
                let nCol = 80;
                let w = 2000;
                let h = 2000;

                //ctx.clearRect(0, 0, 1024, 1024);

                w /= nCol;
                h /= nRow;

                offset = (offset % w) * 2;

                ctx.fillStyle = '#00FF3D';
                ctx.beginPath();
                ctx.rect(0, 0, 2000, 2000);
                ctx.closePath();
                ctx.fill();

                ctx.fillStyle = '#FF00FA';
                ctx.beginPath();
                for (let i = 0; i < nRow; i++) {
                    for (let j = -2; j < nCol; j++) {
                        if (i % 2 === 0) {
                            ctx.rect(j * 2 * w + offset, i * h, w, h);
                        } else {
                            ctx.rect(j * 2 * w + w + offset, i * h, w, h);
                        }
                    }
                }

                ctx.fill();
            }

            let counter = 0;
            let interval = setInterval(function() {
                counter++;
                drawCheckeredBackground(myDynamicTexture.getContext(), counter);
                myDynamicTexture.update();
            }, 32);

            return { mat: myDynamicTexture, interval: interval };
        }*/

        makeDefaultMat();
        makeXrayMat(); // The see through texture
        makeEffectMat(); // The shared effect material for special parts
        makePointEffectMat(); // The shared effect material for special parts
        makePBREffectMat();

        let brain;
        let brainOverlay;
        let e;
        let sensorMat;

        // The first parameter can be used to specify which mesh to import. Here we import all meshes
        BABYLON.SceneLoader.ImportMesh(
            '',
            '/assets/3d/',
            'brain_small_normal_2.babylon',
            scene,
            function(newMeshes) {
                e = scene.createDefaultEnvironment();
                e.layerMask = 0x10000000;
                e.skybox.scalingDeterminant = 10;
                e.skybox.freezeWorldMatrix();

                //Temp REMOVE ME
                sensorMat = newMeshes[1].material; //PLEASE
                sensorMat.fillMode = 3;
                sensorMat.alpha = 0;
                sensorMat.freeze();

                for (let i = 0; i < newMeshes.length; i++) {
                    let mesh = newMeshes[i];

                    //Disable Frustum culling (this would normally be crazy but we are really CPU bound)
                    mesh.cullingStrategy =
                        BABYLON.AbstractMesh.CULLINGSTRATEGY_BOUNDINGSPHERE_ONLY;

                    if (mesh.name === 'brain_pial') {
                        brainOverlay = mesh.clone('brain_overlay');
                        brainOverlay.scaling = new BABYLON.Vector3(
                            1.02,
                            1.02,
                            1.02
                        );
                        brainOverlay.isVisible = true;
                        brainOverlay.freezeWorldMatrix();
                        brainOverlay.setMaterialByID('pointEffectMat');

                        brain = mesh;
                        brain.setMaterialByID('pbrMat');
                        brain.freezeWorldMatrix();
                    } else if (
                        mesh.material &&
                        mesh.material.name === 'lambert3'
                    ) {
                        mesh.scalingDeterminant = 0.75;
                        mesh.freezeWorldMatrix();
                    }
                }
            }
        );

        //Custom function to change brain material
        scene.nmChangeBrain = function(newMat) {
            hemi.setEnabled(false);
            light.setEnabled(false);
            e.ground.setEnabled(true);
            brain.isVisible = true;

            if (newMat === 'transparent') {
                brain.setMaterialByID('xray');
                e.ground.setEnabled(false);
            } else if (newMat === 'disabled') {
                brain.isVisible = false;
                e.ground.setEnabled(false);
            } else if (newMat === 'dynamic') {
                brain.setMaterialByID('effectMat');
            } else if (newMat === 'pbr') {
                hemi.setEnabled(true);
                light.setEnabled(true);
                brain.setMaterialByID('pbrMat');
            } else {
                hemi.setEnabled(true);
                light.setEnabled(true);
                brain.setMaterialByID('defaultMat');
            }
        };
        scene.nmChangeOverlay = function(newOverlay) {
            brainOverlay.isVisible = true;
            brainOverlay.material.emissiveColor = new BABYLON.Color3(
                0.3,
                0.3,
                0.3
            );
            if (newOverlay === 'point') {
                brainOverlay.material.fillMode = 2;
            } else if (newOverlay === 'wireframe') {
                brainOverlay.material.fillMode = 1;
            } else if (newOverlay === 'invisible') {
                brainOverlay.material.fillMode =
                    brainOverlay.material.TriangleStripDrawMode;
                brainOverlay.material.emissiveColor = new BABYLON.Color3(
                    1,
                    1,
                    1
                );
            } else if (newOverlay === 'disabled') {
                brainOverlay.isVisible = false;
            }
        };

        scene.nmSetFullscreen = function(isFullscreen) {
            if (isFullscreen) {
                BABYLON.Tools.RequestFullscreen(canvas);
            } else {
                BABYLON.Tools.ExitFullscreen();
            }
        };

        let defaultPipeline = new BABYLON.DefaultRenderingPipeline(
            'default',
            false,
            scene,
            [camera]
        );

        /////////////////////////
        // POST PROCESSING
        /////////////////////////

        defaultPipeline.fxaaEnabled = true;

        defaultPipeline.imageProcessing.vignetteEnabled = true;
        defaultPipeline.imageProcessing.vignetteWeight = 10;

        defaultPipeline.imageProcessing.toneMappingEnabled = false;

        // Move the light with the camera
        scene.registerBeforeRender(function() {
            if (scene.rotateCamera) {
                camera.alpha += 0.0033;
            }
            light.position = camera.position;
        });

        scene.channelColors = Array.apply(null, Array(Config.queueLength)).map(
            function() {}
        );
        return scene;
    }
}
export default init3D;
