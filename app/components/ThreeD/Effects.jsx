import Config from '../ThreeD/Config';

/* Effects
 * Muscle behind any dynamic effects
 */
class Effects {
    static renderEffectsTexture(ctx, channelColors, percentTime) {
        ctx.fillStyle = '#000';
        ctx.fillRect(
            0,
            0,
            Config.effectTexDimensions[0],
            Config.effectTexDimensions[1]
        );

        let columns = 4;
        let diameter = Math.floor(Config.effectTexDimensions[0] / columns);

        for (let i = channelColors.length; i >= 0; i--) {
            let singleChannelColors = channelColors[Config.queueLength - 1 - i];
            let count = 0;
            for (let key in singleChannelColors) {
                if (singleChannelColors.hasOwnProperty(key)) {
                    let row = Math.floor(count / 4);
                    let col = count % 4;

                    ctx.fillStyle =
                        'rgb(' +
                        singleChannelColors[key][0] * 255 +
                        ',' +
                        singleChannelColors[key][1] * 255 +
                        ',' +
                        singleChannelColors[key][2] * 255 +
                        ',' +
                        singleChannelColors[key][3] +
                        ')'; //Set this 1 for hard edges

                    ctx.beginPath();
                    ctx.arc(
                        diameter * (col + 0.5),
                        diameter * (row + 0.5),
                        (diameter *
                            ((i + percentTime) / (Config.queueLength - 1))) /
                            2,
                        0,
                        2 * Math.PI
                    );
                    ctx.fill();

                    count++;
                }
            }
        }
    }
    static renderOverlayTexture(ctx, channelColors, percentTime) {
        ctx.fillStyle = '#000';
        ctx.fillRect(
            0,
            0,
            Config.effectTexDimensions[0],
            Config.effectTexDimensions[1]
        );
        //ctx.clearRect(0, 0, Config.effectTexDimensions[0], Config.effectTexDimensions[1]);

        let diameter = Math.floor(Config.effectTexDimensions[0] / 10);

        //Sensors location on the UV map for brain_pial as percentages
        let map = Config.realMap;

        for (let i = channelColors.length; i >= 0; i--) {
            let sizeMod =
                Math.pow(i + percentTime, 1) /
                Math.pow(Config.queueLength - 1, 1);
            let opacityMod =
                Math.pow(i + percentTime, 0.5) /
                Math.pow(Config.queueLength - 1, 0.5);

            let singleChannelColors = channelColors[Config.queueLength - 1 - i];
            for (let key in singleChannelColors) {
                if (singleChannelColors.hasOwnProperty(key)) {
                    ctx.fillStyle =
                        'rgba(' +
                        Math.round(singleChannelColors[key][0] * 255) +
                        ',' +
                        Math.round(singleChannelColors[key][1] * 255) +
                        ',' +
                        Math.round(singleChannelColors[key][2] * 255) +
                        ',' +
                        (singleChannelColors[key][3] - opacityMod) +
                        ')'; //Set this 1 for hard edges
                    if (!(key in map)) {
                        console.log('key: ' + key + ' not found in map');
                        continue;
                    }

                    let location = map[key].slice(0);
                    location[0] = location[0] * Config.effectTexDimensions[0];
                    location[1] = location[1] * Config.effectTexDimensions[1];

                    let temporalDiameter = diameter * sizeMod;

                    ctx.beginPath();
                    ctx.arc(
                        location[0],
                        location[1],
                        temporalDiameter,
                        0,
                        2 * Math.PI
                    );
                    ctx.fill();
                }
            }
        }
    }
}
export default Effects;
