import React from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash.throttle';

class ModuleResize extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dragStart: { x: 0, y: 0 },
            isResizing: false
        };

        this.onMouseMoveThrottled = throttle(this.onMouseMove, 50);

        if (typeof window !== 'undefined') {
            window.addEventListener('mouseup', this.onMouseUp);
            window.addEventListener('mousemove', this.onMouseMoveThrottled);
            window.addEventListener('mouseleave', this.onMouseUp);
            window.addEventListener('touchmove', this.onMouseMoveThrottled);
            window.addEventListener('touchend', this.onMouseUp);
        }
    }
    handleClick = (e) => {
        e.preventDefault();
        let clientX = 0;
        let clientY = 0;
        if (e.nativeEvent instanceof MouseEvent) {
            clientX = e.nativeEvent.clientX - e.target.offsetLeft;
            clientY = e.nativeEvent.clientY - e.target.offsetTop;

            //drop if rightclick
            if (e.nativeEvent.which === 3) {
                return;
            }
        } else if (e.nativeEvent instanceof TouchEvent) {
            clientX = e.nativeEvent.touches[0].clientX;
            clientY = e.nativeEvent.touches[0].clientY;
        }
        this.setState({
            dragStart: {
                x: clientX,
                y: clientY
            },
            isResizing: true
        });
    };
    onMouseUp = () => {
        if (!this.state.isResizing) return;
        this.setState({ isResizing: false });
    };
    onMouseMove = (e) => {
        if (!this.state.isResizing) return;

        const clientX =
            e instanceof MouseEvent ? e.clientX : e.touches[0].clientX;
        const clientY =
            e instanceof MouseEvent ? e.clientY : e.touches[0].clientY;

        if (
            clientX < 0 ||
            clientY < 0 ||
            clientX > window.innerWidth ||
            clientY > window.innerHeight
        ) {
            return false;
        }

        let mouseRelative;
        if (this.props.orientation === 'vertical') {
            mouseRelative =
                document
                    .getElementById(
                        this.props.first + '-resize-' + this.props.last
                    )
                    .getBoundingClientRect().left - clientX;
        } else {
            mouseRelative =
                document
                    .getElementById(
                        this.props.first + '-resize-' + this.props.last
                    )
                    .getBoundingClientRect().top - clientY;
        }

        this.props.onResizeStart(
            this.props.first,
            this.props.last,
            mouseRelative
        );

        return false;
    };
    render() {
        return (
            <div
                className={[
                    'ModuleResize',
                    this.props.orientation === 'vertical'
                        ? 'moduleHeightStretch'
                        : null,
                    this.props.orientation
                ].join(' ')}
            >
                <div
                    id={this.props.first + '-resize-' + this.props.last}
                    style={this.state.resizeStyle}
                    onMouseDown={this.handleClick}
                    onTouchStart={this.handleClick}
                    className={[
                        'moduleResizeInner',
                        this.state.isResizing ? 'moving' : 'notMoving'
                    ].join(' ')}
                >
                    <span className="fi flaticon-menu" />
                </div>
            </div>
        );
    }
}
ModuleResize.propTypes = {
    onResizeStart: PropTypes.func,
    orientation: PropTypes.string,
    first: PropTypes.string,
    last: PropTypes.string
};

ModuleResize.defaultProps = {};

export default ModuleResize;
