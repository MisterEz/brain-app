import React from 'react';
import PropTypes from 'prop-types';
import ModuleContainer from './ModuleContainer';
import ModuleResize from './ModuleResize';
import { resizeComponents } from '../Helpers.jsx';

/* Flex(ible) space for moduleContainers
 * Dragable, configurable spaces for modules to go
 */
class Flexspace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //This is an object because the keys must stay the same after delete or move or something
            containers: { 1: { key: 1, module: 'Browser', height: 100 } },

            //width, id
            columns: [[100, 0]],

            //[1, [2,3]] would be one horizontal pane next to two vertical panes
            panes: [[1]],
            isDragging: false,
            isResizing: false,
            resizeTimer: null
        };
        this.lastKey = 3;
        this.lastColKey = 1;
    }
    handleVertResizerCallbacks = (
        first,
        last,
        mouseRelative,
        recursionLock
    ) => {
        let upPercentDelta = (mouseRelative / window.innerHeight) * 100;
        let downPercentDelta = -upPercentDelta;

        let firstMod = this.state.containers[first];
        let lastMod = this.state.containers[last];

        let firstModHeight = firstMod.height - upPercentDelta;
        let lastModHeight = lastMod.height - downPercentDelta;

        // If the resize will push past minimum size find the smallest acceptable change
        // and retry this function
        if (firstModHeight < 5 && !recursionLock) {
            let newMouseRelative =
                ((-5 + firstMod.height) / 100) * window.innerHeight;
            this.handleVertResizerCallbacks(
                first,
                last,
                newMouseRelative,
                true
            );
            return false;
        } else if (lastModHeight < 5 && !recursionLock) {
            let newMouseRelative =
                ((5 - lastMod.height) / 100) * window.innerHeight;
            this.handleVertResizerCallbacks(
                first,
                last,
                newMouseRelative,
                true
            );
            return false;
        }

        if (firstModHeight > 100 || lastModHeight > 100) {
            return false;
        }

        // I wonder if people have been fired for doing this.
        // but it saves a deep copy. (you aren't supposed to edit state directly)
        this.state.containers[first].height = firstModHeight;
        this.state.containers[last].height = lastModHeight;

        window.clearTimeout(this.state.resizeTimer);
        let resizeTimer = window.setTimeout(() => {
            this.setState({
                isResizing: false,
                isDragging: false
            });
        }, 100);

        this.setState({
            containers: this.state.containers,
            isResizing: true,
            isDragging: true,
            resizeTimer: resizeTimer
        });
    };
    handleHoriResizerCallbacks = (
        first,
        last,
        mouseRelative,
        recursionLock
    ) => {
        let leftPercentDelta = (mouseRelative / window.innerWidth) * 100;
        let rightPercentDelta = -leftPercentDelta;

        if (
            first < 0 ||
            last < 0 ||
            first >= this.state.columns.length ||
            last >= this.state.columns.length
        ) {
            return;
        }

        let firstMod = this.state.columns[first][0];
        let lastMod = this.state.columns[last][0];

        // If trying to make a min size object smaller
        // look for siblings to resize instead
        if (
            lastMod === 4 &&
            rightPercentDelta > 0 &&
            parseInt(last) + 1 <= this.state.columns.length
        ) {
            this.handleHoriResizerCallbacks(
                first,
                parseInt(last) + 1,
                mouseRelative
            );
            return false;
        } else if (
            firstMod === 4 &&
            leftPercentDelta > 0 &&
            parseInt(first) - 1 >= 0
        ) {
            this.handleHoriResizerCallbacks(
                parseInt(first) - 1,
                last,
                mouseRelative
            );
            return false;
        }

        let firstModWidth = firstMod - leftPercentDelta;
        let lastModWidth = lastMod - rightPercentDelta;

        // If the resize will push past minimum size find the smallest acceptable change
        // and retry this function
        if (firstModWidth < 4 && !recursionLock) {
            let newMouseRelative = ((-4 + firstMod) / 100) * window.innerWidth;
            this.handleHoriResizerCallbacks(
                first,
                last,
                newMouseRelative,
                true
            );
            return false;
        } else if (lastModWidth < 4 && !recursionLock) {
            let newMouseRelative = ((4 - lastMod) / 100) * window.innerWidth;
            this.handleHoriResizerCallbacks(
                first,
                last,
                newMouseRelative,
                true
            );
            return false;
        }

        // should never happen but mouseUp event is unreliable
        if (firstModWidth > 100 || lastModWidth > 100) {
            return false;
        }

        this.state.columns[first][0] = firstModWidth;
        this.state.columns[last][0] = lastModWidth;

        window.clearTimeout(this.state.resizeTimer);
        let resizeTimer = window.setTimeout(() => {
            this.setState({
                isResizing: false,
                isDragging: false
            });
        }, 100);

        this.setState({
            columns: this.state.columns,
            isResizing: true,
            isDragging: true,
            resizeTimer: resizeTimer
        });
    };
    findInPanes = (id) => {
        //returns column index in this.state.panes
        let panes = this.state.panes;

        for (let i = 0; i < panes.length; i++) {
            let pane = panes[i];
            for (let j = 0; j < pane.length; j++) {
                if (pane[j].toString() === id) {
                    return [i, j, pane.length > 1];
                }
            }
        }
        throw 'findInPanes: Could not find id: ' + id + ' in panes!';
    };
    handleContainerCallback = (action) => {
        if ('close' in action && action.close in this.state.containers) {
            //Check if only moduleContainer
            if (Object.keys(this.state.containers).length <= 1) {
                return false;
            }

            //find in panes
            let findColArr = this.findInPanes(action.close);
            let colIdx = findColArr[0];
            let rowIdx = findColArr[1];
            let isVertical = findColArr[2];

            let containers = this.state.containers;
            let panes = this.state.panes;
            let columns = this.state.columns;

            delete containers[action.close];

            if (isVertical) {
                //resize parner
                let partnerId = panes[colIdx][1 - rowIdx];
                containers[partnerId].height = 100;

                //remove vertically
                panes[colIdx].splice(rowIdx, 1);
            } //horizontal
            else {
                //remove from columns
                columns.splice(colIdx, 1);

                //remove from panes
                panes.splice(colIdx, 1);

                //resize everything in columns
                //calculate the resize ratio
                let totalWidth = 0;
                for (let i = 0; i < columns.length; i++) {
                    totalWidth += columns[i][0];
                }
                let ratio = 100 / totalWidth;

                //apply resize ratio
                for (let i = 0; i < columns.length; i++) {
                    columns[i][0] *= ratio;
                }
            }

            window.clearTimeout(this.state.resizeTimer);
            let resizeTimer = window.setTimeout(() => {
                this.setState({
                    isResizing: false
                });
            }, 300);
            this.setState({
                containers: containers,
                panes: panes,
                columns: columns,
                isResizing: true,
                resizeTimer: resizeTimer
            });
        }
        if ('hsplit' in action && action.hsplit in this.state.containers) {
            let panes = this.state.panes;

            // check panes.length
            if (panes.length >= 3) {
                return; //It's too wide already
            }

            // find column in panes
            let colIdx = this.findInPanes(action.hsplit)[0];

            let ratio = 0.75; // 75/100
            //Width subtracted from new container. Useful when some columns are too small to shrink
            let pixelSqueeze = 0;
            let newColumns = this.state.columns;

            //Resize columns
            for (let i = 0; i < newColumns.length; i++) {
                let newWidth = newColumns[i][0] * ratio;
                if (newWidth < 4) {
                    pixelSqueeze = 4 - newWidth;
                    newWidth = 4;
                }
                newColumns[i][0] = newWidth;
            }
            let newId = ++this.lastKey;
            let newWidth = 25 - pixelSqueeze;

            // Insert new width into columns
            newColumns.splice(colIdx + 1, 0, [newWidth, ++this.lastColKey]);

            // Insert new id into panes
            panes.splice(colIdx + 1, 0, [newId]);

            // Add new module to container
            let containers = this.state.containers;
            containers[newId] = {
                key: newId,
                module: 'Browser',
                height: 100
            };

            window.clearTimeout(this.state.resizeTimer);
            let resizeTimer = window.setTimeout(() => {
                this.setState({
                    isResizing: false
                });
            }, 300);
            this.setState({
                containers: containers,
                columns: newColumns,
                panes: panes,
                isResizing: true,
                resizeTimer: resizeTimer
            });
        }
        if ('vsplit' in action && action.vsplit in this.state.containers) {
            let panes = this.state.panes;

            // find column in panes
            let colIdx = this.findInPanes(action.vsplit)[0];

            // check pane vertical length
            if (panes[colIdx].length > 1) {
                return;
            }

            //resize containers
            let containers = this.state.containers;
            containers[action.vsplit].height = 50;

            let newId = ++this.lastKey;

            // put ID in panes vertically
            panes[colIdx].push(newId);

            // Add new module to container
            containers[newId] = {
                key: newId,
                module: 'Browser',
                height: 50
            };

            window.clearTimeout(this.state.resizeTimer);
            let resizeTimer = window.setTimeout(() => {
                this.setState({
                    isResizing: false
                });
            }, 300);
            this.setState({
                containers: containers,
                panes: panes,
                isResizing: true,
                resizeTimer: resizeTimer
            });
        }
    };
    render() {
        let components = [];

        //Iterate through this.state.panes
        for (let i = 0; i < this.state.panes.length; i++) {
            let column = this.state.panes[i];
            let colComponents = [];

            if (i > 0) {
                components.push(
                    <ModuleResize
                        key={'vert' + i}
                        orientation="vertical"
                        first={i - 1 + ''}
                        last={i + ''}
                        onResizeStart={this.handleHoriResizerCallbacks}
                    />
                );
            }

            let width = this.state.columns[i][0];
            for (let j = 0; j < column.length; j++) {
                let containerId = column[j];
                let el = this.state.containers[containerId];

                if (j > 0) {
                    let lastContainerId = column[j - 1];
                    let lastEl = this.state.containers[lastContainerId];

                    colComponents.push(
                        <ModuleResize
                            key={'hori' + lastEl.key + ''}
                            orientation="horizontal"
                            first={lastEl.key.toString()}
                            last={el.key.toString()}
                            onResizeStart={this.handleVertResizerCallbacks}
                        />
                    );
                }

                colComponents.push(
                    <ModuleContainer
                        key={el.key.toString()}
                        id={el.key.toString()}
                        initModule={el.module}
                        width={width}
                        height={el.height}
                        isResizing={this.state.isResizing}
                        containerCallback={this.handleContainerCallback}
                        solo={Object.keys(this.state.containers).length <= 1}
                        hextendable={this.state.columns.length < 3}
                        vextendable={column.length < 2}
                    />
                );
            }

            components.push(
                <div
                    className="moduleColumn"
                    key={'col' + this.state.columns[i][1]}
                    style={{ width: width + '%' }}
                >
                    {colComponents}
                </div>
            );
        }
        return (
            <div
                className={[
                    'Flexspace',
                    this.props.visible ? 'visible' : 'hidden',
                    this.state.isResizing ? 'resizing' : 'notResizing',
                    this.state.isDragging ? 'dragging' : 'notDragging'
                ].join(' ')}
            >
                <div className="flexspace_row">{components}</div>
            </div>
        );
    }
    componentDidUpdate() {
        if (typeof resizeComponents === 'function') {
            resizeComponents();
        }
    }
    //So this could be done in CSS but javascript height handling is in preperation of multiple rows
    componentDidMount() {
        resizeComponents();
        window.onresize = resizeComponents;
    }
}

Flexspace.propTypes = {
    visible: PropTypes.bool
};

export default Flexspace;
