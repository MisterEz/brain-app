import React from 'react';
import PropTypes from 'prop-types';
import Battery from './Status/Battery';
import Connection from './Status/Connection';
import SensorQuality from './Status/SensorQuality';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    handleFlipProjectorMode = (e) => {
        e.preventDefault();

        this.props.menuCallback({
            projectorMode: !this.props.projectorMode
        });
    };
    handleAbout = (e) => {
        e.preventDefault();
        this.setState({
            isOpen: false
        });
        this.props.menuCallback({
            openWindow: 'about'
        });
    };
    handleHelp = (e) => {
        e.preventDefault();
        this.setState({
            isOpen: false
        });
        this.props.menuCallback({
            openWindow: 'help'
        });
    };
    handleToggle = (e) => {
        e.preventDefault();
        this.setState((state) => {
            return {
                isOpen: !state.isOpen
            };
        });
    };

    render() {
        let imgSrc = window.Compatibility.isMobile
            ? 'assets/img/logos/Brainstorm@105.png'
            : 'assets/img/logos/Brainstorm@27.png';
        return (
            <div className="Menu">
                <div id="skiptocontent">
                    <a href="#maincontent">skip to main content</a>
                </div>
                <header>
                    <nav className="navbar navbar-default">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <button
                                    type="button"
                                    className="navbar-toggle collapsed"
                                    onClick={this.handleToggle}
                                >
                                    <span className="sr-only">
                                        Toggle navigation
                                    </span>
                                    <span className="icon-bar" />
                                    <span className="icon-bar" />
                                    <span className="icon-bar" />
                                </button>
                                <a className="navbar-brand" href="/">
                                    <img src={imgSrc} alt="Brainstorm" />
                                </a>
                            </div>{' '}
                            {/* .navbar-header */}
                            <div
                                className={[
                                    'navbar-collapse',
                                    this.state.isOpen
                                        ? 'collapse in'
                                        : 'collapse'
                                ].join(' ')}
                                id="bs_navbar_collapse"
                            >
                                <ul className="nav navbar-nav">
                                    <li>
                                        <a href="#" onClick={this.handleAbout}>
                                            About
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onClick={this.handleHelp}>
                                            Help
                                        </a>
                                    </li>
                                    <li role="separator" className="divider" />
                                </ul>
                                <ul className="nav navbar-nav navbar-right">
                                    <li className="noHeadsetOnly">
                                        <p className="navbar-text alert-danger">
                                            No headset detected
                                        </p>
                                    </li>
                                    <li className="statusIcon headsetOnly">
                                        <Battery size={0} />
                                    </li>
                                    <li className="statusIcon headsetOnly">
                                        <Connection size={0} />
                                    </li>
                                    <li className="statusIcon headsetOnly">
                                        <SensorQuality size={0} />
                                    </li>
                                    <li>
                                        <a
                                            onClick={
                                                this.handleFlipProjectorMode
                                            }
                                            href="#"
                                        >
                                            {this.state.projectorMode
                                                ? 'Desktop Mode'
                                                : 'Projector Mode️'}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            {/* #bs_navbar_collapse */}
                        </div>
                        {/* .container */}
                    </nav>
                </header>
            </div>
        );
    }
}

Menu.propTypes = {
    menuCallback: PropTypes.func,
    projectorMode: PropTypes.bool
};

Menu.defaultProps = {};

export default Menu;
