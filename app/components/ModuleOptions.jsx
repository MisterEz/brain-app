import React from 'react';
import PropTypes from 'prop-types';

/* Options for modules!
 */
class ModuleOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            hidden: true
        };
        this.optionsRef = React.createRef();
        this.lastOpen = false;
        this.transitionTimeout;
    }
    handleChange = (e) => {
        if (this.props.optionsCallback) {
            let value = e.target.value;
            if (e.target.type === 'checkbox') {
                value = e.target.checked ? true : false;
            } else if (e.target.type === 'range') {
                value = Number(e.target.value);
            }
            this.props.optionsCallback({
                [e.target.name]: value
            });
        }
    };
    handleClose = () => {
        this.setState({
            open: false
        });

        this.transitionTimeout = setTimeout(
            function() {
                this.setState((state) => ({
                    hidden: !state.open
                }));
            }.bind(this),
            250
        );
    };
    handleOpen = () => {
        this.setState({
            hidden: false
        });

        let els = this.optionsRef.current.querySelectorAll('.moSubMenu');
        [].forEach.call(els, function(el) {
            el.classList.add('moHidden');
        });

        //workaround for transitions to work after being hidden or displayed
        setTimeout(
            function() {
                this.setState({ open: true });
            }.bind(this),
            10
        );
    };
    handleSubmenuChange = (e, id, shouldRemove) => {
        e.preventDefault();
        let el = this.optionsRef.current.querySelector('#' + id);
        if (shouldRemove) {
            el.classList.remove('moHidden');
        } else {
            el.classList.add('moHidden');
        }
    };
    componentDidUpdate() {
        if (this.lastOpen !== this.props.open) {
            this.lastOpen = this.props.open;
            if (this.state.open) {
                this.handleClose();
            } else {
                this.handleOpen();
            }
        }
    }
    render() {
        let getClassNames = function(el) {
            let classNames = [];
            if (el.className !== undefined) {
                classNames = el.className.split(' ');
            }
            return classNames;
        };

        let makeCheckbox = function(el) {
            let classNames = getClassNames(el);
            if (el.checked === undefined) {
                el.checked = false;
            }
            return (
                <div className="moSection" key={el.name}>
                    <label className="checkboxLabel">
                        <h4>{el.text}</h4>
                        {el.desc}
                        <input
                            className={['checkbox_' + el.name]
                                .concat(classNames)
                                .join(' ')}
                            type="checkbox"
                            name={el.name}
                            checked={el.value}
                            value="true"
                            onChange={el.onChange || this.handleChange}
                        />
                    </label>
                </div>
            );
        }.bind(this);

        let makeSelectable = function(el) {
            if (el.checked === undefined) {
                el.checked = false;
            }
            return (
                <div
                    className="moSection2"
                    id={el.id}
                    key={el.name}
                    type="selectable"
                    isselected={el.isselected ? 1 : 0}
                    draggable={el.draggable}
                    onDragStart={el.onDragStart}
                    onDrop={el.onDrop}
                    onDrag={el.onDrag}
                    onClick={el.onClick}
                    ref={el.ref}
                >
                    <label id={el.id} type="selectable">
                        <h4 id={el.id} type="selectable">
                            {el.text}
                        </h4>
                    </label>
                </div>
            );
        }.bind(this);

        let makeRanger = function(el) {
            let classNames = getClassNames(el);

            if (el.min === undefined) {
                el.min = 0;
            }
            if (el.max === undefined) {
                el.max = 100;
            }

            return (
                <div className="moSection" key={el.name}>
                    <label>
                        <h4>{el.text}</h4>
                        {el.desc}
                        <input
                            className={['slider_' + el.name]
                                .concat(classNames)
                                .join(' ')}
                            type="range"
                            name={el.name}
                            min={el.min}
                            max={el.max}
                            value={el.value}
                            onChange={el.onChange || this.handleChange}
                        />
                    </label>
                </div>
            );
        }.bind(this);

        let makeButton = function(el) {
            let classNames = getClassNames(el);
            if (classNames.length < 1) {
                classNames.push('btn-default');
            }
            if (el.active) {
                classNames.push('active');
            }
            return (
                <button
                    className={['btn', 'button_' + el.name]
                        .concat(classNames)
                        .join(' ')}
                    name={el.name}
                    type="button"
                    key={el.key || el.name}
                    value={el.value}
                    onClick={el.onClick}
                >
                    {el.text}
                </button>
            );
        }.bind(this);

        let makeSectionButton = function(el) {
            let classNames = getClassNames(el);

            let flair = null;
            if (el.flair) {
                flair = (
                    <span
                        className={['moFlair', 'fi', el.flair].join(' ')}
                        aria-hidden="true"
                    />
                );
            }

            return (
                <div
                    className={['moSection'].concat(classNames).join(' ')}
                    key={el.name}
                >
                    <a className="expandy" href="#!" onClick={el.onClick} />
                    <h4>{el.text}</h4>
                    <div className="moText">{el.desc}</div>
                    {flair}
                </div>
            );
        }.bind(this);

        let makeDownloadButton = function(el) {
            let classNames = getClassNames(el);
            if (classNames.length < 1) {
                classNames.push('btn-default');
            }
            if (el.active) {
                classNames.push('active');
            }
            return (
                <a
                    href={el.href}
                    download={el.download}
                    onClick={el.onClick}
                    key={el.key || el.name}
                    type="downloadbutton"
                >
                    {el.text}
                </a>
            );
        }.bind(this);

        let makeButtonGroup = function(el) {
            let classNames = getClassNames(el);

            let btns = [];
            for (let j = 0; j < el.btns.length; j++) {
                let btn = Object.assign({}, el.btns[j]);
                if (btn.name === el.value) {
                    btn.className += 'btn-default active';
                }

                btn.onClick = this.handleChange;
                btn.value = btn.name;
                btn.key = btn.name;
                btn.name = el.name;
                btns.push(makeButton(btn));
            }
            return (
                <div className="moSection" key={el.name}>
                    <h4>{el.text}</h4>
                    <div className="moText">{el.desc}</div>
                    <div
                        className={['btn-group', 'buttonGroup_' + el.name]
                            .concat(classNames)
                            .join(' ')}
                        role="group"
                    >
                        {btns}
                    </div>
                </div>
            );
        }.bind(this);

        let getElement = function(el) {
            let classNames = getClassNames(el);

            if (el.type === 'checkbox') {
                return makeCheckbox(el);
            } else if (el.type === 'ranger') {
                return makeRanger(el);
            } else if (el.type === 'button') {
                return makeButton(el);
            } else if (el.type === 'downloadbutton') {
                return makeDownloadButton(el);
            } else if (el.type === 'buttonGroup') {
                return makeButtonGroup(el);
            } else if (el.type === 'selectable') {
                return makeSelectable(el);
            }
            // Ayy
            else if (el.type === 'list') {
                let listEls = arrayToEls(el.list);
                return (
                    <div className="moList" key={el.name}>
                        {listEls}
                    </div>
                );
            } else if (el.type === 'sectionButton') {
                return makeSectionButton(el);
            } else if (el.type === 'submenu') {
                if (el.visible === null) {
                    el.visible = false;
                }
                if (!el.returnButtonMade) {
                    el.options.unshift({
                        type: 'sectionButton', //Text [X]
                        name: 'moSubMenuOpen-' + el.name,
                        className: 'moSubBack',
                        text: (
                            <span>
                                <span
                                    className="fi flaticon-arrowhead-thin-outline-to-the-left"
                                    aria-hidden="true"
                                />
                                {el.text}
                            </span>
                        ),
                        onClick: (e) =>
                            this.handleSubmenuChange(
                                e,
                                'moSubMenu-' + el.name,
                                false
                            )
                    });
                    el.returnButtonMade = true;
                }
                let els = arrayToEls(el.options);

                let sectionButton = makeSectionButton({
                    name: el.name,
                    text: el.text,
                    desc: el.desc,
                    flair: 'flaticon-arrow-point-to-right',
                    onClick: (e) =>
                        this.handleSubmenuChange(
                            e,
                            'moSubMenu-' + el.name,
                            true
                        )
                });

                return [
                    sectionButton,
                    <div
                        key={'moSubMenu-' + el.name}
                        id={'moSubMenu-' + el.name}
                        className={[
                            'moSubMenu',
                            el.visible ? 'moVisible' : 'moHidden'
                        ].join(' ')}
                    >
                        {els}
                    </div>
                ];
            }
            //Too trivial to make reusable functions
            else if (el.type === 'heading') {
                return (
                    <h3
                        className={['heading_' + name]
                            .concat(classNames)
                            .join(' ')}
                        key={el.name}
                    >
                        {el.text}
                    </h3>
                );
            } else if (el.type === 'text') {
                return (
                    <p
                        className={['moText', 'text_' + name]
                            .concat(classNames)
                            .join(' ')}
                        key={el.name}
                    >
                        {el.text}
                    </p>
                );
            } else if (el.type === 'br') {
                return (
                    <br
                        className={['br_' + name].concat(classNames).join(' ')}
                        key={el.name}
                    />
                );
            } else if (el.type === 'hr') {
                return (
                    <hr
                        className={['hr_' + name].concat(classNames).join(' ')}
                        key={el.name}
                    />
                );
            }
        }.bind(this);

        let normalizeEl = function(el, i) {
            if (el.name === undefined) {
                el.name = el.type + i;
            }
            if (el.value === undefined) {
                el.value = '';
            }
            return el;
        }.bind(this);

        let arrayToEls = function(options) {
            let els = [];
            for (let i = 0; i < options.length; i++) {
                let el = options[i];
                el = normalizeEl(el, i);

                let returnedEl = getElement(el);
                if (Array.isArray(returnedEl)) {
                    els = els.concat(returnedEl);
                } else {
                    els.push(returnedEl);
                }
            }
            return els;
        }.bind(this);

        let els = arrayToEls(this.props.options);
        let openStateClassName = this.state.open ? 'moOpen' : 'moClose';
        return (
            <div>
                <div
                    ref={this.optionsRef}
                    className={['ModuleOptions', openStateClassName].join(' ')}
                    style={{ display: this.state.hidden ? 'none' : 'block' }}
                >
                    <div className="moHeader">
                        <h2>Settings</h2>
                        <button
                            type="button"
                            className="close"
                            aria-label="Close"
                            onClick={this.handleClose}
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="moBody">
                        <form>{els}</form>
                    </div>
                </div>

                {this.props.showButton ? (
                    <button
                        style={{
                            display: !this.state.hidden ? 'none' : 'block'
                        }}
                        className={[
                            'moduleOptionsOpenBtn',
                            'btn',
                            'btn-primary',
                            openStateClassName
                        ].join(' ')}
                        onClick={this.handleOpen}
                    >
                        <span
                            className="fi flaticon-gear-option"
                            aria-hidden="true"
                        />
                    </button>
                ) : null}
            </div>
        );
    }
    componentWillUnmount() {
        window.clearTimeout(this.transitionTimeout);
    }
}

ModuleOptions.propTypes = {
    optionsCallback: PropTypes.func,
    options: PropTypes.array,
    open: PropTypes.bool,
    showButton: PropTypes.bool
};

ModuleOptions.defaultProps = {
    options: [],
    showButton: false,
    open: false
};

export default ModuleOptions;
