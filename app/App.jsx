import React from 'react';
import Menu from './components/Menu';
import HelpWindow from './components/HelpWindow';
import Flexspace from './components/Flexspace';
import io from 'socket.io-client';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectorMode: false,
            socketConnection: false,
            orientation: 'portrait',
            openWindow: null
        };

        this.fakeBciInt = null;
        this.bciUpdateCount = 0;
        this.bciLastTime = 0;

        if (typeof window !== 'undefined') {
            window.addEventListener('resize', this.detectOrientation);
        }
    }
    handleMenuCallback = (newState) => {
        this.setState(newState);
    };
    detectOrientation = () => {
        let orientation = 'portrait';
        if (window.innerHeight < window.innerWidth) {
            orientation = 'landscape';
        }
        this.setState({
            orientation: orientation
        });
    };
    simulateBciUpdates = () => {
        this.fakeBciInt = window.setInterval(() => {
            if (this.state.socketConnection) {
                return;
            }
            let channels = {};
            for (let i = 0; i < window.bciConfig.channels.length; i++) {
                let channel = window.bciConfig.channels[i];
                channels[channel] = {
                    theta: Math.random() * 100,
                    alpha: Math.random() * 100,
                    lowbeta: Math.random() * 100,
                    highbeta: Math.random() * 100,
                    gamma: Math.random() * 100
                };
            }
            this.sendBciUpdate(channels);
        }, 100);
    };
    sendBciUpdate = (channels) => {
        let frame = {
            id: this.bciUpdateCount++,
            time: Date.now(),
            interval: Date.now() - this.bciLastTime,
            channels: channels
        };
        let event = new CustomEvent('bciUpdate', { detail: frame });
        window.dispatchEvent(event);
        this.bciLastTime = Date.now();
    };
    sendSensorsUpdate = (sensors) => {
        let frame = {
            time: Date.now(),
            sensors: sensors
        };
        let event = new CustomEvent('sensorsUpdate', { detail: frame });
        window.dispatchEvent(event);
    };
    sendBatteryUpdate = (data) => {
        let frame = {
            time: Date.now(),
            batteryLevel: data.batteryLevel,
            batteryMax: data.maxBatteryLevel
        };
        let event = new CustomEvent('batteryUpdate', { detail: frame });
        window.dispatchEvent(event);
    };
    sendConnectionUpdate = (data) => {
        let frame = {
            time: Date.now(),
            signalStrength: data.signalStrength
        };
        let event = new CustomEvent('connectionUpdate', { detail: frame });
        window.dispatchEvent(event);
    };
    render() {
        return (
            <div
                className={[
                    'App',
                    this.state.projectorMode ? 'projectorZoom' : 'defaultZoom',
                    this.state.socketConnection ? 'headset' : 'noHeadset',
                    this.state.orientation,
                    window.Compatibility.isMobile ? 'mobile' : 'desktop'
                ].join(' ')}
            >
                <Menu
                    menuCallback={this.handleMenuCallback}
                    projectorMode={this.state.projectorMode}
                />
                <main id="maincontent">
                    <HelpWindow
                        menuCallback={this.handleMenuCallback}
                        show={this.state.openWindow}
                    />

                    <div className="containerFluid">
                        <Flexspace visible={!this.state.openWindow} />
                    </div>
                </main>
            </div>
        );
    }
    componentDidMount() {
        window.bciConfig = {
            channels: [
                'AF3',
                'F7',
                'F3',
                'FC5',
                'T7',
                'P7',
                'O1',
                'O2',
                'P8',
                'T8',
                'FC6',
                'F4',
                'F8',
                'AF4'
            ],
            frequencies: ['theta', 'alpha', 'lowbeta', 'highbeta', 'gamma'],
            simulated: true
        };
        //event 'bciConfigChange' will be fired if window.bci_config is changed
        this.simulateBciUpdates();
        this.socket = io(
            window.location.protocol +
                '//' +
                window.location.hostname +
                ':8080/bci'
        );
        this.socket.on(
            'connect',
            function() {
                this.setState({
                    socketConnection: true
                });

                //has optional param data
                clearInterval(this.fakeBciInt);
                window.bciConfig.simulated = false;
            }.bind(this)
        );
        this.socket.on('bciUpdate', (data) => {
            this.sendBciUpdate(data);
        });
        this.socket.on('sensorsUpdate', (data) => {
            this.sendSensorsUpdate(data);
        });
        this.socket.on('batteryUpdate', (data) => {
            this.sendBatteryUpdate(data);
        });
        this.socket.on('connectionUpdate', (data) => {
            this.sendConnectionUpdate(data);
        });

        this.detectOrientation();
    }
}
export default App;
