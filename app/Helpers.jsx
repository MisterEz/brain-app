export function resizeComponents() {
    let headerHeight = document.getElementsByTagName('header')[0].offsetHeight;

    let windowHeight =
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight;

    let modules = document.getElementsByClassName('moduleHeightStretch');

    let desiredHeight = windowHeight - headerHeight;

    Array.prototype.forEach.call(modules, function(el) {
        let heightAdd = 0;
        let height = 1;
        if (el.hasAttribute('data-height')) {
            height = el.dataset.height / 100;
        } else if (
            !(
                window.Compatibility.isMobile &&
                window.innerHeight < window.innerWidth
            )
        ) {
            //if portrait
            heightAdd = -50;
        }
        el.style.cssText =
            'height: ' +
            (Math.floor(desiredHeight * height) + heightAdd) +
            'px;';
    });
}
