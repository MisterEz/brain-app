import 'babel-polyfill';
import 'raf';
import React from 'react';
import ReactDOM from 'react-dom';

import Compatibility from './Compatability.jsx';
import App from './App';

let compatibilityHandler = new Compatibility();

compatibilityHandler.makeCompatibile();
window.Compatibility = compatibilityHandler;

ReactDOM.render(<App />, document.getElementById('root'));
